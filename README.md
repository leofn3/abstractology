Abstractology is a python module that automates a methodology to do semantic
analysis, classification and discovery of structural shifts in large corpora
employing stochastic block modeling (SBM) from graph-tool or word embedding
from Gensim.

In general, using this library requires previous installation of:
- [graph-tool](graph-tool.skewed.de/)
- [gensim](https://radimrehurek.com/gensim/)
- [bokeh](https://bokeh.org/)

From a user's perspective, this module is composed of two main classes:

`Graphology`:
  corpus processing (inherited from `Corporalogy`)
  from processed corpus to SBM partition of a corpus
  from SBM partition to visualisations and reports (inherited from `Blockology`)

`Vectorology`:
  corpus processing (inherited from `Corporalogy`)
  from processed corpus to word embedding and document scores
  from document scores to visualisations and reports (inherited from `Scorology`)

