# coding: utf-8

# Abstractology - Study of the organisation and evolution of a corpus
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Project:
# <https://en.wikiversity.org/wiki/The_dynamics_and_social_organization_of
#  _innovation_in_the_field_of_oncology>
#
# Reference repository for this file:
# <https://gitlab.com/solstag/abstractology>
#
# Contributions are welcome, get in touch with the author(s).

"""
Abstractology - toolkit to study the organisation and evolution of corpora
=============

Requirements
------------

This package may require, depending on the task:
- Numpy, Pandas, Scipy
- Bokeh, Matplotlib
- Gensim, Lxml, Nltk

About
-----

This module provides four main classes:

`class Corporalogy`
To load and preprocess corpora.

`class Graphology`
To model a corpora using document-word networks and produce
reports, statistics and visualisations.

`class Zmethods`
Functions to calculate interesting quantities in `Graphology` visualisations.

`class Vectorology`
To model a corpora using word embedding and produce
reports, statistics and visualisations.

Example
-------

from abstractology import Graphology, Zmethods

a = Graphology('sample_corpus')
a.load_corpus(source='datadir/sample_corpus.csv', parser='csv')


# Create a document-word graph, fit the model and load it
a.create_graphs()
gname = a.loaded['graphs'][-1]
snames = a.calc_nested_blockstates(gname)
a.load_blockstate(snames[0])
a.blockstate_to_dataframes()

# create an interactive visualisation of the model
a.plot_block_level_matrix( ['doc', 'voc'],
    norm=['bylevelmax', 'bylevelmax'],
    scale=['linear', 'linear'],
    zmethod=[Zmethods.density, Zmethods.t_doc_density])

a.register_loaded() # Once registered, all parameters and the data get
                    # autoloaded on class instantiation by passing only
                    # the fist (`analysis_dir`) argument.
# On a future run...

from abstractology import Graphology, Zmethods
a = Graphology('sample_corpus')
gname = a.loaded['graphs'][-1]

(...)


"""

__author__ = "Alexandre Hannud Abdo <abdo@member.fsf.org>"
__copyright__ = "Copyright 2016-2019 Alexandre Hannud Abdo"
__license__ = "GNU GPL version 3 or above"
__URL__ = "https://gitlab.com/solstag/abstractology/"


__all__ = ['Corporalogy', 'Graphology', 'Vectorology', 'Zmethods']


import re, os, pickle, json, gzip
import pandas
from os import path
from functools import partial
from itertools import chain
from tempfile import NamedTemporaryFile

#####################
# Property helpers  #
#####################

def _getp(p, self):
    return getattr(self, p)

def _setp(p, self, val):
    setattr(self, p, val)

def makep(p, getp=_getp, setp=_setp, delp=None, docp=None):
    p = '_{}'.format(p)
    pargs = [getp, setp, delp, docp]
    for i, arg in enumerate(pargs):
        if arg is not None:
            pargs[i] = partial(arg, p)
    return pargs

def property_getfuncdir(func):
    def g(self):
        val = func(self)
        os.makedirs(val, exist_ok=True)
        return val
    return property(g)

def _setp_dir(p, self, val):
    val = path.normpath(val)
    os.makedirs(val, exist_ok=True)
    setattr(self, p, val)

def checkp(self, p):
    try: getattr(self, p)
    except AttributeError: return False
    return True

def clearattrs(obj, names):
    for name in names:
        try:
            delattr(obj, name)
        except AttributeError:
            pass

###################
# Naming utilities #
###################

class naming():
    esc, itemsep, valsep, pathsep = '~', '; ', '=', '_'
    seps = [esc, itemsep, valsep, pathsep, path.sep, path.extsep]
    _count = 0
    for x in seps:
        for y in seps:
            _count += x in y
    assert _count==len(seps)

    @classmethod
    def escape(cls, name):
        """
        Escape the path separator and argument delimiter in names.
        The escape char gets escaped if we're sure it is not already escaping.
        """
        name = re.sub('(?<!['+cls.itemsep[0]+cls.pathsep+cls.esc+'])'
                      + cls.esc + '(?!['+cls.esc+'])',
                      2*cls.esc, name)
        name = re.sub(cls.itemsep, cls.itemsep[0]+cls.esc, name)
        name = re.sub(path.sep, cls.pathsep+cls.esc, name)
        return name

    @classmethod
    def check(cls, *args):
        forbidden = (cls.esc, path.sep, cls.itemsep, cls.valsep)
        if any(y in x for x in args for y in forbidden):
            raise ValueError

    @classmethod
    def gen(cls, base, params, ext):
        params = [ tuple(map(str, x)) for x in params ]
        cls.check(*chain(*params), ext)
        base = cls.escape(base)
        params = ( map(cls.escape, x) for x in params )
        name = cls.itemsep.join(chain([base], map(cls.valsep.join, params)))
        return path.extsep.join([name, ext.lstrip(path.extsep)])

    @classmethod
    def splitext(cls, fname):
        name, ext = path.splitext(fname)
        while ext[1:] in ('gz', 'xz', 'bz2', 'lzma', 'zip'):
            name, ext = path.splitext(name)
        return name, fname[len(name):]


################
# I/O utilities #
################

class ioio():
    formatters = {
        'pickle':{
            'module': pickle,
            'rmode':'rb',
            'wmode':'wb',
            'rmethod':'read_pickle',
            'wmethod':'to_pickle',
            'extra_args':{},
            'compress_args':{'compression':'gzip'}
        },
        'json':{
            'module': json,
            'rmode':'rt',
            'wmode':'wt',
            'rmethod':'read_json',
            'wmethod':'to_json',
            'extra_args':{'orient':'split'},
            'compress_args':{'compression':'gzip'}
        },
        'hdf5':{
            'module': None,
            'rmode':'rb',
            'wmode':'wb',
            'rmethod':'read_hdf',
            'wmethod':'to_hdf',
            'extra_args':{'key':'singleton'},
            # don't compress as it seems not to work
            'compress_args':{}
            #'compress_args':{'complib':'blosc:zlib', 'complevel':9}
        }
    }

    @classmethod
    def load(cls, fname, fdir=path.curdir, format=None, formatter_args={}):
        """
        Reads an object from the disk.

        Parameters
        ----------
        fname: string
            Name of file to read from.
        fdir: string
            Name of directory to read the file from.
        format: string
            One of pickle or json; if `None`, tries to guess from extension;
            defaults to pickle.
        formatter_args: dict
            Parameters passed to reading function.

        Returns
        -------
        The object read
        """
        if format is None:
            ext = path.splitext(fname)[-1][1:]
            format = ext if ext in cls.formatters else 'pickle'
        formatter = cls.formatters[format]['module']
        mode = cls.formatters[format]['rmode']

        for fopen in gzip.open, open:
            try:
                with fopen(path.join(fdir, fname), mode) as f:
                    return formatter.load(f, **formatter_args)
            except OSError as e:
                if 'Not a gzipped file' in ''.join(map(str, e.args)): pass
                else: raise e

    @classmethod
    def store(cls, obj, fname, fdir=path.curdir, format=None,
              formatter_args={}, compress=True):
        """
        Stores an object to the disk.

        Parameters
        ----------
        obj: object
            The object to be stored.
        fname: string
            Name of file to save to.
        fdir: string
            Name of directory to save the file in, will be created if needed.
        format: string
            One of pickle or json; if `None`, tries to guess from extension;
            defaults to pickle.
        formatter_args: dict
            Parameters passed to writing function.
        compress: bool
            If available, whether to compress the result.
        """
        if format is None:
            ext = path.splitext(fname)[-1][1:]
            format = ext if ext in cls.formatters else 'pickle'
        formatter = cls.formatters[format]['module']
        mode = cls.formatters[format]['wmode']

        os.makedirs(path.dirname(path.join(fdir, fname)), exist_ok=True)
        fopen = gzip.open if compress else open
        with fopen(path.join(fdir, fname), mode) as f:
            formatter.dump(obj, f, **formatter_args)

    @classmethod
    def load_pandas(cls, fname, fdir=path.curdir, format=None,
                    formatter_args={}):
        """
        Reads a pandas object from the disk.

        Parameters
        ----------
        fname: string
            name of file to read from.
        fdir: string
            Name of directory to read the file from.
        format: string
            One of pickle or json; if `None`, tries to guess from extension;
            defaults to pickle.
        formatter_args: dict
            Parameters passed to reading function.

        Returns
        -------
        A pandas object, usually a Series or Dataframe
        """
        if format is None:
            ext = path.splitext(fname)[-1][1:]
            format = ext if ext in cls.formatters else 'pickle'
        method = cls.formatters[format]['rmethod']
        compress_args = cls.formatters[format]['compress_args']
        extra_args = cls.formatters[format]['extra_args']

        # compression in `to_hdf` seems useless, we might have used gzip on top
        if format=='hdf5':
            with NamedTemporaryFile() as tf:
                for fopen in gzip.open, open:
                    try:
                        with fopen(path.join(fdir, fname), 'rb') as f:
                            tf.write(f.read())
                    except OSError as e:
                        if 'Not a gzipped file' in ''.join(map(str, e.args)):
                            pass
                        else:
                            raise e
                return getattr(pandas, method)(tf.name, **formatter_args)

        try:
            df = getattr(pandas, method)(path.join(fdir, fname),
                **dict(chain( extra_args.items(),
                              formatter_args.items(),
                              compress_args.items() )))
        except OSError as e:
            if 'Not a gzipped file' in ''.join(map(str, e.args)):
                df = getattr(pandas, method)(path.join(fdir, fname),
                    **dict(chain(extra_args.items(), formatter_args.items())) )
            else:
                raise e

        # json doesn't handle tuples, so we must find and convert our tokens
        if format!='json':
            return df
        df = df.transform( lambda x:
            x.map(lambda y: tuple(map(tuple, y)), na_action='ignore')
            if all( type(y) is list
                    and all( type(z) is list
                             and all( type(w) is str for w in z )
                             for z in y )
                    for y in x if pandas.isna(y) is not True ) # not redundant!
            else x )
        return df

    @classmethod
    def store_pandas(cls, obj, fname, fdir=path.curdir, format=None,
                     formatter_args={}, gzipped=True):
        """
        Stores a pandas object to the disk.

        Parameters
        ----------
        obj: object
            The object to be stored.
        fname: string
            Name of file to save to.
        fdir: string
            Name of directory to save the file in, will be created if needed.
        format: string
            One of pickle, hdf or json; if `None`, tries to guess from
            extension; defaults to pickle.
        formatter_args: dict
            Parameters passed to writing function.
        gzipped: bool
            Force gzip compression, overriding inference and `formatter_args`.
        """
        if format is None:
            ext = path.splitext(fname)[-1][1:]
            format = ext if ext in cls.formatters else 'pickle'
        method = cls.formatters[format]['wmethod']
        extra_args = cls.formatters[format]['extra_args']
        compress_args = cls.formatters[format]['compress_args']

        formatter_args = dict(chain(
            extra_args.items(),
            formatter_args.items(),
            compress_args.items() if gzipped is True else () ))

        os.makedirs(path.dirname(path.join(fdir, fname)), exist_ok=True)
        getattr(obj, method)(path.join(fdir, fname), **formatter_args)

        # compression in `to_hdf` seems useless, let's also use gzip on top
        if format=='hdf5' and gzipped:
            with open(path.join(fdir, fname), 'rb') as f:
                data = f.read()
            with gzip.open(path.join(fdir, fname), 'wb') as g:
                g.write(data)

################
# lxml helpers #
################

def display_html_in_browser(el, title='Corporalogister'):
    import webbrowser
    doc = build.HTML( build.HEAD( build.TITLE(title) ) )
    doc.append( build.BODY( el ) )
    with NamedTemporaryFile() as f:
        f.write(html.tostring(doc))
        f.flush()
        webbrowser.open(f.name)
        input('Hold until the browser gets a chance to load the file!')

##########################
# Main classes available #
##########################

from . corporalogy import Corporalogy
from . graphology import Graphology
from . blockology.zmethods import Zmethods
from . vectorology import Vectorology
