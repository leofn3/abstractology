# coding: utf-8

# Abstractology - Study of the organisation and evolution of a corpus
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Project:
# <https://en.wikiversity.org/wiki/The_dynamics_and_social_organization_of
#  _innovation_in_the_field_of_oncology>
#
# Reference repository for this file:
# <https://gitlab.com/solstag/abstractology>
#
# Contributions are welcome, get in touch with the author(s).

import graph_tool.all as gt
import multiprocessing, numpy, os, pandas, pickle
from functools import lru_cache, partial
from itertools import combinations, chain
from heapq import nsmallest
from collections import OrderedDict, Counter, defaultdict
import colorcet
from scipy.spatial.distance import pdist
from matplotlib import pyplot as plt, colors
from matplotlib.backends.backend_pdf import PdfPages
import bokeh.plotting as bkp, bokeh.models as bkm, bokeh.layouts as bkl
import bokeh.palettes as bkpa, bokeh.events as bke
from bokeh.models.callbacks import CustomJS
from tqdm import tqdm

from .. import naming
from .block_level_map import plot_block_level_map
from .zmethods import Zmethods

# import pdb; # pdb.set_trace()


class Blockology:
    """
        Build graphs from corpora or vectors and find optimal block models.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if "title" not in self.labels:
            raise KeyError("`title` must be set in property `labels`")
        if self.labels["title"] is None:
            print("Warning, `title` in property `labels` is None")

    def to_hierarchical_block(self, level, block):
        for blocks, levels in (
            (self.blocks, self.blocks_levels),
            (self.wblocks, self.wblocks_levels),
            (self.eblocks, self.eblocks_levels),
        ):
            b = blocks.loc[blocks[level] == block, levels[levels.index(level) :]]
            if not b.empty:
                return tuple(reversed(b.iloc[0].tolist()))

    def to_block_label(self, level, block):
        """
        Parameters
        ----------
        `level`: the block's level; `None` if `block` is hierarchical tuple
        `block`: int or hierarchical tuple
        """

        if isinstance(block, tuple):
            assert level is None
            level = len(self.blocks_levels) - len(block) + 1
        else:
            assert level is not None
        for blocks, levels, sep in (
            (self.blocks, self.blocks_levels, "D"),
            (self.wblocks, self.wblocks_levels, "T"),
            (self.eblocks, self.eblocks_levels),
            "E",
        ):
            b = blocks.loc[blocks[level] == block, levels[levels.index(level) :]]
            if not b.empty:
                return "L{}{}{}".format(level, sep, b.iloc[0].iloc[0])

    def get_blocks_levels_sample(self, btype):
        if btype == "doc":
            blocks, levels = self.blocks, self.blocks_levels
            sblocks = self.blocks.loc[self.data.index]
        elif btype == "ter":
            blocks, levels = self.wblocks, self.wblocks_levels
            sblocks = self.wblocks.loc[
                self.wblocks.index.intersection(self.get_vocab())
            ]
        elif btype == "ext":
            blocks, levels = self.eblocks, self.eblocks_levels
            sblocks = blocks  # TODO actually sample eblocks
        else:
            raise ValueError("Unrecognized `bytpe`.")
        return blocks, levels, sblocks

    def find_blocks(self, lscape, finder):
        """
        Returns the blocks that correspond to features defined in finder.
        For example, blocks with the highest or lowest values, or with
        the highest or lowest differences in values across the landscape.

        Parameters
        ----------
        lscape: dict of pandas.Series or pandas.DataFrames
            The landscape of values for each level over its blocks.
        finder: str or function
            Finds the desired blocks in the landscape.

        Returns
        -------
        found: dict
            Dictionary containing whatever `finder` looked for.
        """
        if callable(finder):
            return finder(self, lscape)

        found = dict()
        if finder == "level_max_min_absmin":
            for level, ls_l in lscape.items():
                if isinstance(ls_l, pandas.Series):
                    found[level] = dict(
                        max=ls_l.idxmax, min=ls_l.idxmin, absmin=ls_l.abs().idxmin()
                    )
                elif isinstance(ls_l, pandas.DataFrame):
                    idxmax = ls_l.max().idxmax()
                    idxmin = ls_l.min().idxmin()
                    idxabsmin = ls_l.abs().min().idxmin()
                    found[level] = dict(
                        max=(ls_l.idxmax()[idxmax], idxmax),
                        min=(ls_l.idxmin()[idxmin], idxmin),
                        absmin=(ls_l.abs().idxmin()[idxabsmin], idxabsmin),
                    )
                else:
                    raise ValueError("Unrecognized type in values of `lscape`.")
        return found

    def plot_sashimi(self, title, annotate=False, diff_idxs=None, chained=False):
        idx_all = self.data.index.copy()
        idx_all.name = "all"

        if not chained and not diff_idxs:
            return plot_block_level_map(
                self,
                ["doc", "ter"],
                diff=[False, True],
                norm=["bylevelmax", "bylevelmax"],
                scale=["linear", "log"],
                zmethod=[
                    Zmethods.density,
                    Zmethods.x_doc_density_pair_gen(
                        idx_all, idx_all, Zmethods.p_rel, "ter"
                    ),
                ],
                link_p_func=Zmethods.p_rel,
                annotate=annotate,
                page_head=title,
            )

        if not chained and diff_idxs:
            idx0, idx1 = diff
            return plot_block_level_map(
                self,
                ["doc", "ter"],
                diff=[True, True],
                norm=["bylevelmax", "bylevelmax"],
                scale=["linear", "log"],
                zmethod=[
                    Zmethods.density_pair_gen(idx0, idx1, Zmethods.p_diff),
                    Zmethods.x_doc_density_pair_gen(
                        idx_all, idx_all, Zmethods.p_rel, "ter"
                    ),
                ],
                link_p_func=Zmethods.p_rel,
                annotate=annotate,
                page_head=title,
            )

        if chained and not diff_idxs:
            return plot_block_level_map(
                self,
                ["ext", "doc"],
                diff=[True, False],
                norm=["bylevelmax", "bylevelmax"],
                scale=["log", "linear"],
                zmethod=[
                    Zmethods.x_doc_density_pair_gen(
                        idx_all, idx_all, Zmethods.p_rel, "ext"
                    ),
                    Zmethods.density,
                ],
                link_p_func=Zmethods.p_rel,
                annotate=annotate,
                page_head=title,
            )

        if chained and diff_idxs:
            idx0, idx1 = diff
            return plot_block_level_map(
                self,
                ["ext", "doc"],
                diff=[True, True],
                norm=["bylevelmax", "bylevelmax"],
                scale=["log", "linear"],
                zmethod=[
                    Zmethods.x_doc_density_pair_gen(
                        idx_all, idx_all, Zmethods.p_rel, "ext"
                    ),
                    Zmethods.density_pair_gen(idx0, idx1, Zmethods.p_diff),
                ],
                link_p_func=Zmethods.p_rel,
                annotate=annotate,
                page_head=title,
            )

    @lru_cache(maxsize=None)
    def get_dblock_wordcount(self, b):
        level = len(self.blocks_levels) - len(b) + 1
        bdocs = self.data.loc[self.blocks[level] == b[-1], self.column]
        #        c = Counter(w for d in bdata for w in sum(d, ()))
        c = Counter(chain(*bdocs.map(lambda x: sum(x, ())).values))
        return c

    def get_dblock_words(self, b, order, n=10):
        wc = self.get_dblock_wordcount(b)
        if order == "local_specificity":
            if len(b) > 1:
                upwc = self.get_dblock_wordcount(b[:-1])
            else:
                upwc = wc

            def keyf(x):
                return -wc[x] * numpy.log1p(wc[x]) / upwc[x]

        elif order == "global_specificity":
            if len(b) > 1:
                topwc = self.get_dblock_wordcount(b[:1])
            else:
                topwc = wc

            def keyf(x):
                return -wc[x] * numpy.log1p(wc[x]) / topwc[x]

        elif order == "frequency":

            def keyf(x):
                return -wc[x]

        return nsmallest(n, wc, key=keyf)

    def get_wblock_words(self, b, order="frequency", n=10, wblocks=None):
        wblocks = self.wblocks if wblocks is None else wblocks
        level = len(self.wblocks_levels) - len(b) + 1
        wb = wblocks[wblocks[level] == b[-1]]

        if order == "sample":
            return wb.sample(min(n, wb.index.size)).index

        grade, _, dfreq = self.get_graded_vocab_cached(self.column, sampled=True)
        if order == "concentration":
            keyf = lambda x: -grade[x]
        elif order == "frequency":
            keyf = lambda x: -dfreq[x]

        #            words = [w for w in wblocks[wblocks[level]==b[-1]].index
        #                     if w in grade]
        #            notwords = [w for w in wblocks[wblocks[level]==b[-1]].index
        #                        if w not in grade]
        #            if notwords: print('Missing words:', notwords)

        return nsmallest(n, wb.index, key=keyf)

    @lru_cache(maxsize=None)
    def get_wblock_doccount(self, b):
        level = len(self.wblocks_levels) - len(b) + 1
        bwords = self.wblocks[self.wblocks[level] == b[-1]].index
        #        c = Counter(d for w in bwords for d in self.ter_documents[w])
        c = Counter(chain(*bwords.map(self.ter_documents.__getitem__).values))
        return c

    def get_wblock_docs(self, b, order="global_specificity", n=10):
        dc = self.get_wblock_doccount(b)
        if order == "local_specificity":
            if len(b) > 1:
                updc = self.get_wblock_doccount(b[:-1])
            else:
                updc = dc

            def keyf(x):
                return -dc[x] / updc[x]

        if order == "global_specificity":
            if len(b) > 1:
                topdc = self.get_wblock_doccount(b[:1])
            else:
                topdc = dc

            def keyf(x):
                return -dc[x] / topdc[x]

        elif order == "universal specificity":
            unidc = self.get_wblock_doccount(())

            def keyf(x):
                return -dc[x] / unidc[x]

        return nsmallest(n, dc, key=keyf)

    def filter_word_block_from_corpus(self, b, column=None):
        """
        Removes all words belonging to word block "b" from the corpus.
        """
        level = len(self.wblocks_levels) - len(b) + 1
        in_block = self.wblocks[level] == b[-1]
        words_to_remove = set(self.wblocks[in_block].index)
        return self.filter_words(lambda w: w not in words_to_remove, column)
