# coding: utf-8

# Abstractology - Study of the organisation and evolution of a corpus
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Project:
# <https://en.wikiversity.org/wiki/The_dynamics_and_social_organization_of
#  _innovation_in_the_field_of_oncology>
#
# Reference repository for this file:
# <https://gitlab.com/solstag/abstractology>
#
# Contributions are welcome, get in touch with the author(s).

import graph_tool.all as gt
import multiprocessing, numpy, os, pandas, pickle
from functools import lru_cache, partial
from itertools import combinations, chain
from heapq import nsmallest
from collections import OrderedDict, Counter, defaultdict
import colorcet
from scipy.spatial.distance import pdist
from matplotlib import pyplot as plt, colors
from matplotlib.backends.backend_pdf import PdfPages
import bokeh.plotting as bkp, bokeh.models as bkm, bokeh.layouts as bkl
import bokeh.palettes as bkpa, bokeh.events as bke
from bokeh.models.callbacks import CustomJS
from tqdm import tqdm

from .. import naming
from .util import get_sorted_hierarchical_block_index
from .zmethods import Zmethods


def get_block_level_landscape(self, btype, zmethod, zby=None, zrel=None, zargs={}):
    """
    Applies zmethod over all blocks for each level.

    Parameters
    ----------
    btype: string
        Whether to apply to document or word blocks.
    zmethod: function
        The function which returns the series of values for a level.
    zrel: 2-tuple of named pandas indexes
        Calculate values for the first index relative to the second.
    zby: object to groupby
        Splits blocks into subgroups and evaluate consecutive pairs.
    zargs: dict
        Extra arguments passed to `zmethod`.

    Returns
    -------
    lscape: dict of pandas.DataFrames
        for each level, the series of values over its blocks.
    """
    blocks, levels, sblocks = self.get_blocks_levels_sample(btype)

    lscape = dict()

    if zrel is not None:
        for level in levels:
            lidx = blocks[level].unique()
            lscape[level] = zmethod(zrel[0], zrel[1], **zargs)(
                self, sblocks, level, lidx
            )
        return lscape

    elif zby is not None:
        idxs = []
        for gname, g in self.data.groupby(zby):
            g.index.name = gname
            idxs.append(g.index)
        for level in levels:
            lscape[level] = pandas.DataFrame(index=blocks[level].unique())
            for idx0, idx1 in zip(idxs[1:], idxs):
                lscape[level][(idx0.name, idx1.name)] = zmethod(idx0, idx1, **zargs)(
                    self, sblocks, level, lscape[level].index
                )
        return lscape

    else:
        for level in levels:
            lidx = blocks[level].unique()
            lscape[level] = zmethod(self, sblocks, level, lidx)
        return lscape


def plot_block_level_map_s(
    self,
    btype,
    zmethod,
    title,
    norm=None,
    scale="linear",
    bheight="hierarchical",
    palette=None,
    diff=False,
    annotate=True,
    force_norm_diff=True,
):
    """
    This method returns a bokeh.figure object to be further processed.
    You're probably looking for 'plot_block_level_map'.
    """
    print("{} (annotate: {})".format(btype, annotate))
    blocks, levels, sblocks = self.get_blocks_levels_sample(btype)
    lscape = get_block_level_landscape(self, btype, zmethod)

    source = dict(
        (k, [])
        for k in (
            "x",
            "y",
            "width",
            "height",
            "z",
            "block",
            "value",
            "freq_words",
            "gspec_words",
            "lspec_words",
            "samp_words",
            "line_width",
            "count",
            "scount",
            "titles",
        )
    )
    b2h = {(): 1}

    for level in tqdm(reversed(levels[:]), total=len(levels), desc="Level"):
        # print('Level: {}'.format(level))

        bindex = get_sorted_hierarchical_block_index(blocks, levels, level)

        # total height
        h_sum = (
            lscape[level].index.size
            if bheight == "equal"
            else blocks[level].index.size
            if bheight == "procount"
            else lscape[level].sum()
            if bheight == "proval"
            else 1
        )
        h_cum = 0

        # nomalization factor
        znorm = 1
        scaler = numpy.log if (diff and scale == "log") else lambda x: x
        lscape_ls = lscape[level].apply(scaler)
        if norm == "bylevel":
            znorm = lscape_ls.loc[numpy.isfinite(lscape_ls)].abs().sum()
        elif norm == "bylevelmax":
            znorm = lscape_ls.loc[numpy.isfinite(lscape_ls)].abs().max()

        for b in tqdm(bindex, desc="Block"):
            # print('Block: {}'.format(b))
            val = lscape[level].loc[b[-1]]
            count = blocks[blocks[level] == b[-1]].index.size
            scount = sblocks[sblocks[level] == b[-1]].index.size

            h = (
                1 / h_sum
                if bheight == "equal"
                else count / h_sum
                if bheight == "procount"
                else val / h_sum
                if bheight == "proval"
                else None
            )
            if bheight == "hierarchical":
                num_same_parent = (
                    1
                    if len(b) == 1
                    else blocks.loc[blocks[level + 1] == b[-2], level].unique().size
                )
                h = b2h[b[:-1]] / num_same_parent
                b2h[b] = h

            source["x"].append(level)
            source["y"].append(h_cum + h / 2)
            source["width"].append(1)
            source["height"].append(h)
            source["block"].append(str(b))
            source["count"].append(count)
            source["scount"].append(scount)
            source["line_width"].append(h)
            h_cum += h

            # bokeh can't handle infs
            if val == numpy.inf:
                source["value"].append("inf")
            elif val == -numpy.inf:
                source["value"].append("-inf")
            elif numpy.isnan(val):
                source["value"].append(1)
            else:
                source["value"].append(val)

            # GAMBI: scale manually, bokeh.LogColorMapper doesn't center
            if diff and scale == "log":
                source["z"].append(numpy.log(val) / (znorm or 1))
            else:
                source["z"].append(val / (znorm or 1))

            if annotate and btype == "doc":
                freq_words = self.get_dblock_words(b, "frequency", 10)
                gspec_words = self.get_dblock_words(b, "global_specificity", 10)
                lspec_words = self.get_dblock_words(b, "local_specificity", 10)
                titles = self.data.loc[(sblocks[level] == b[-1]), self.labels["title"]]
                source["titles"].append(
                    " | ".join(
                        titles
                        if titles.empty
                        else titles.sample(min(10, titles.size)).astype(str)
                    )
                )
                source["freq_words"].append(", ".join(freq_words))
                source["gspec_words"].append(", ".join(gspec_words))
                source["lspec_words"].append(", ".join(lspec_words))

            elif annotate and btype == "ter":
                freq_words = self.get_wblock_words(b, "frequency", 10, sblocks)
                samp_words = self.get_wblock_words(b, "sample", 10, sblocks)
                titles = self.data.loc[self.get_wblock_docs(b), self.labels["title"]]
                source["titles"].append(" | ".join(titles.astype(str)))
                source["freq_words"].append(", ".join(freq_words))
                source["samp_words"].append(", ".join(samp_words))

    figheight = min(900, 512 + 4 * blocks[levels].agg(lambda s: len(s.unique())).max())
    fig = bkp.figure(
        toolbar_location=None,
        tools="",
        title=title,
        width=942,
        height=figheight,
        x_range=(max(levels) + 0.5, min(levels) - 0.5),
        y_range=(0, 1),
        x_axis_label="level",
        y_axis_label="blocks",
    )
    fig.grid.visible = False
    fig.xaxis[0].ticker.desired_num_ticks = len(levels)
    fig.xaxis.minor_tick_line_color = None
    fig.title.text_font_size = "17px"
    fig.title.align = "center"
    fig.title_location = "above"

    if not force_norm_diff:
        cmap_low = min(filter(numpy.isfinite, source["z"]))
        cmap_high = max(filter(numpy.isfinite, source["z"]))
        if diff and scale == "log":  # GAMBI: bokeh zuado
            cmap_absmax = max(abs(cmap_low), abs(cmap_high), 1)
            cmap_low, cmap_high = -cmap_absmax, cmap_absmax
            scale = "linear"
            # cmap_absmax = max(abs(1/cmap_low), abs(cmap_high))
            # cmap_low, cmap_high = 1/cmap_absmax, cmap_absmax
        elif diff:
            cmap_absmax = max(abs(cmap_low), abs(cmap_high))
            cmap_low, cmap_high = -cmap_absmax, cmap_absmax
        elif scale == "linear" and cmap_low > 0:
            cmap_low = 0
        if cmap_low == cmap_high:
            cmap_high += 0.001
            if diff and scale == "linear":
                cmap_low -= 0.001
    else: # KISS
        cmap_low, cmap_high = -1, 1
        scale = 'linear'

    palette_color = (
        colorcet.b_diverging_bwr_40_95_c42[128:]
        if btype == "doc"
        else list(reversed(colorcet.b_diverging_bwr_40_95_c42[:128]))
        if btype == "ter"
        else list(reversed(colorcet.b_diverging_bwr_40_95_c42[:128]))
        if btype == "ext"
        else None
    )
    palette_high, palette_low = "violet", "black"
    if palette is None:
        if diff or force_norm_diff:
            palette = colorcet.dimgray[124:-4] + palette_color
        else:
            palette = palette_color
    elif palette is "positive":
        palette = 129 * ["#ffffff"] + palette_color
        palette_low = "#ffffff"
    elif palette is "negative":
        palette = colorcet.dimgray[128:] + 129 * ["#ffffff"]
        palette_high = "#ffffff"

    cmapper = bkm.LogColorMapper if scale == "log" else bkm.LinearColorMapper
    cmap = cmapper(
        palette,
        low=cmap_low,
        high=cmap_high,
        high_color=palette_high,
        low_color=palette_low,
    )
    line_color = "black" if "#ffffff" in palette else "white"

    # bokeh can't handle infs
    for i, v in enumerate(source["z"]):
        if v == numpy.inf:
            source["z"][i] = cmap_high + 1
        if v == -numpy.inf:
            source["z"][i] = cmap_low - 1
        if numpy.isnan(v):  # gambi?
            source["z"][i] = 0 if (diff and scale == "log") else 1

    # bkm.ColumnDataSource doesn't handle unused columns
    for key in [k for k in source if not source[k]]:
        source.pop(key)

    source = bkm.ColumnDataSource(data=source, name='{}_map_data'.format(btype))
    fig.rect(
        "x",
        "y",
        "width",
        "height",
        source=source,
        line_width="line_width",
        line_color=line_color,
        fill_color={"field": "z", "transform": cmap},
        selection_line_width=4.0,
        selection_line_color={
            "doc": "slateblue",
            "ter": "firebrick",
            "ext": "firebrick",
        }[btype],
        nonselection_alpha=1.0,
    )

    if max(v.index.size for v in lscape.values()) > 1:
        ticker = {"ticker": bkm.LogTicker()} if scale == "log" else {}
        color_bar = bkm.ColorBar(color_mapper=cmap, location=(0, 0), **ticker)
        color_bar.major_label_text_font_size = "17px"
        fig.add_layout(color_bar, "right")

    return block_level_map_add_tools(self, btype, fig, annotate)


def block_level_map_add_tools(self, btype, fig, annotate):
    source = fig.select_one('{}_map_data'.format(btype))

    # HTML templates for tooltip and infobox

    head_html = """<span style="color:#3333dd">block:</span> {block}"""
    tooltip_num_html = """
        <span style="color:#3333dd">value:</span> @value{{0,0.0[00]}}
        <span style="color:#3333dd">color:</span> @z{{0,0.0[00]}}
        <span style="color:#3333dd">count:</span> @scount / @count"""
    info_num_html = """
        <span style="color:#3333dd">value:</span> {value}<br />
        <span style="color:#3333dd">color:</span> {z}<br />
        <span style="color:#3333dd">count:</span> {scount} / {count}"""
    term_html = (
        """
        <span style="color:#3333dd">globally specific terms:</span><br />
        {gspec_words}<br />
        <span style="color:#3333dd">locally specific terms:</span><br />
        {lspec_words}"""
        if btype == "doc"
        else """
        <span style="color:#3333dd">frequent terms:</span><br />
        {freq_words}<br />
        <span style="color:#3333dd">sample terms:</span><br />
        {samp_words}"""
    )
    title_html = (
        """
        <span style="color:#3333dd">sample titles:</span><br />
        {titles}"""
        if btype == "doc"
        else """
        <span style="color:#3333dd">globally specific titles:</span><br />
        {titles}"""
    )
    if not annotate:
        term_html = title_html = ""

    # HoverTool - tooltip

    tooltip_wrap = """<div style="max-width:450px; padding:5px; font-size: 17px">
        <p>{}<br />{}<br />{}</p></div>"""
    tooltip_sub = dict(
        block="@block",
        gspec_words="@gspec_words",
        lspec_words="@lspec_words",
        freq_words="@freq_words",
        samp_words="@samp_words",
    )
    tooltip_html = tooltip_wrap.format(head_html, tooltip_num_html, term_html).format(
        **tooltip_sub
    )
    fig.add_tools(
        bkm.HoverTool(
            point_policy="follow_mouse", tooltips=tooltip_html, attachment="horizontal"
        )
    )

    # WheelzoomTool

    wheelzoom_tool = bkm.WheelZoomTool(
        dimensions="height", maintain_focus=True, zoom_on_axis=True
    )
    wheelzoom_callback = CustomJS(
        args=dict(source=source),
        code="""
        var plot = cb_obj.origin
        if(plot.y_range.start<0){ plot.y_range.start=0; }
        if(plot.y_range.end>1){ plot.y_range.end=1; }
        var index = source.inspected["1d"].indices[0]
        var iheight = source.data.height[index];
        var iend = source.data.y[index] + iheight/2;
        var istart = source.data.y[index] - iheight/2;
        if(plot.y_range.start>istart){ plot.y_range.start=istart; }
        if(plot.y_range.end<iend){ plot.y_range.end=iend; }
        """,
    )
    fig.js_on_event(bke.MouseWheel, wheelzoom_callback)
    fig.add_tools(wheelzoom_tool)
    fig.toolbar.active_scroll = wheelzoom_tool

    # Infobox

    infobox = bkm.Div()
    infobox_wrap = """<div style="width:896px; margin:23px; font-size:
        17px"><p>{}<br /><br />{}<br /><br />{}<br /><br />{}</p></div>"""
    infobox_html = infobox_wrap.format(head_html, info_num_html, term_html, title_html)
    infobox_callback = CustomJS(
        args=dict(source=source, infobox=infobox, infobox_html=infobox_html),
        code="""
        var index = source.inspected["1d"].indices[0]
        infobox.text = infobox_html.replace(/{[\A-z ]*}/g, function(x){
            x = source.data[x.slice(1, -1)][index]
            y = String(x).match(/-?\d+\.\d{4}/)
            return y ? y[0] : x })
        """,
    )
    fig.js_on_event(bke.Tap, infobox_callback)

    return fig, infobox


def plot_block_level_map(
    self,
    btype,
    zmethod=None,
    desc=None,
    norm=None,
    scale="linear",
    bheight="hierarchical",
    palette=None,
    diff=False,
    by=None,
    annotate=True,
    link_p_func=None,
    page_head=None,
):
    """
        Colormap to display aggregates at different levels.
        Colors can represent, for example, journals/authors/institutions
        within, or crossing, blocks at that given level.

        Colors are taken from the z axis, which is calculated from
        'zmethod', a member of the abstract Zmethod class. See the
        documentation for that class for more information and to define
        your own functions.

        When sampling the data with 'set_sample()':
        * the block structure is not affected
        * displayed value, color, words and titles are affected
        * when calling 'zmethod', 'blocks' is affected but 'index' is not
        * displayed count shows both sample and total as a fraction

        You may pass a list of two or more btypes, in this case multiple
        colormaps will be displayed using the options given. Options that
        are also lists will then be applied to the corresponing map.
    """
    wargs = locals().copy()
    for key in ("self", "link_p_func", "page_head"):
        wargs.pop(key)
    wargs["title"] = ""
    if isinstance(wargs.pop("btype"), str):
        btype = [btype]
    if isinstance(wargs["palette"], list) and wargs["palette"][0][0] == "#":
        wargs["palette"] = [wargs["palette"]] * len(btype)
    for arg in wargs:
        if not isinstance(wargs[arg], list):
            wargs[arg] = [wargs[arg]] * len(btype)

    title_txt = "{btype} map: {desc} " "(norm {norm}, scale {scale}, bheight {bheight})"
    name_args = "by", "norm", "scale", "bheight"
    bt_name = {"doc": "Domain", "ter": "Topic"}
    if "ext" in btype:
        bt_name["ext"] = self.graph_extended["prop"]

    for i in range(len(btype)):
        if wargs["zmethod"][i] is None:
            wargs["zmethod"][i] = Zmethods.count
        if wargs["desc"][i] is None:
            wargs["desc"][i] = wargs["zmethod"][i].__name__
        elif callable(wargs["desc[i]"]):
            wargs["desc"][i] = wargs["desc"][i](wargs["zmethod"][i].__name__)
        wargs["title"][i] = title_txt.format(
            btype=bt_name[btype[i]], **dict((k, wargs[k][i]) for k in wargs)
        ).capitalize()

    fname = naming.gen(
        "{btype}_block_level_map_{desc}".format(btype=btype, **wargs),
        [(key, wargs[key]) for key in name_args],
        "html",
    )
    page_title = title_txt.format(
        btype=list(map(bt_name.get, btype)), **wargs
    ).capitalize()

    def internal(btype, by, desc, **kwargs):
        if by is None:
            self.cache_clear()
            fig, info = plot_block_level_map_s(self, btype, **kwargs)
            return fig, info
        else:
            tabs = list()
            for t in sorted(self.data[by].unique()):
                print("Plotting for {by}: {t}".format(by=by, t=t))
                bykwargs = kwargs.copy()
                odata = self.data
                try:
                    self.data = self.data[self.data[by] == t]
                    bykwargs["title"] = "{title} by {by}".format(by=by, **bykwargs)
                    self.cache_clear()
                    fig = plot_block_level_map_s(self, btype, **bykwargs)
                    tabs.append(bkm.widgets.Panel(child=fig, title=str(t), width=942))
                finally:
                    self.data = odata
            return bkm.widgets.Tabs(tabs=tabs, width=942)

    rows = [bkm.Div(height=90, text=page_head)]
    figs, infos = zip(
        *(
            internal(btype[i], **dict((k, wargs[k][i]) for k in wargs))
            for i in range(len(btype))
        )
    )
    if len(btype) == 1:
        figrow = bkm.Row(figs[0], infos[0])

    if btype == ["doc", "ter"] and wargs["by"] == [None, None]:
        link_maps(
            self,
            figs[0],
            self.blocks,
            self.blocks_levels,
            figs[1],
            self.wblocks,
            self.wblocks_levels,
            self.values_ter_doc_count_all,
            "source",
            pfunc=link_p_func,
        )
        link_maps(
            self,
            figs[1],
            self.wblocks,
            self.wblocks_levels,
            figs[0],
            self.blocks,
            self.blocks_levels,
            self.values_ter_doc_count_all,
            "target",
            pfunc=link_p_func,
        )
        tabs0 = bkm.widgets.Tabs(
            width=942,
            tabs=[
                bkm.widgets.Panel(child=figs[0], title="Domain map"),
                bkm.widgets.Panel(child=infos[1], title="Topic info"),
            ],
        )
        tabs1 = bkm.widgets.Tabs(
            width=942,
            tabs=[
                bkm.widgets.Panel(child=figs[1], title="Topic map"),
                bkm.widgets.Panel(child=infos[0], title="Domain info"),
            ],
        )
        figrow = bkm.Row(tabs0, tabs1)

    if btype == ["ext", "doc"] and wargs["by"] == [None, None]:  # TODO all
        link_maps(
            self,
            figs[1],
            self.blocks,
            self.blocks_levels,
            figs[0],
            self.eblocks,
            self.eblocks_levels,
            self.values_ext_doc_count_all,
            "target",
            pfunc=link_p_func,
        )
        tabs0 = bkm.widgets.Tabs(
            width=942,
            tabs=[
                bkm.widgets.Panel(child=figs[0], title="Chained map"),
                bkm.widgets.Panel(child=infos[1], title="Domain info"),
            ],
        )
        tabs1 = bkm.widgets.Tabs(
            width=942,
            tabs=[
                bkm.widgets.Panel(child=figs[1], title="Domain map"),
                bkm.widgets.Panel(child=infos[0], title="Chained info"),
            ],
        )
        figrow = bkm.Row(tabs0, tabs1)

    rows.insert(1, figrow)
    bkp.output_file(
        os.path.join(self.blocks_adir, fname), title=page_title, mode="inline"
    )
    return bkp.save(bkl.column(rows))


def link_maps(
    self,
    source_fig,
    source_blocks,
    source_levels,
    target_fig,
    target_blocks,
    target_levels,
    values,
    doc_position,
    pfunc=None,
    norm="bylevelmax",
):
    """
    Once linked, clicking on a block in the source map will produce a change on the target map's z-axis.

    Currently this implements an x_doc_density_pair calculation with pfunc against all.

    Tested with norm='bylevelmax' and pfunc=p_rel (diff='log).

    Parameters
    ==========
    values:
        A `pandas.Series` of scalar values with `MultiIndex` like
        (source level, source block, target level, target block)
    """
    if doc_position not in ('source', 'target'):
        raise ValueError('`doc_position` must be either "source" or "target"')

    # plot boundary limits
    if pfunc is None:

        def st_val_map(x):
            return x

        def st_z_map(x):
            return x

    elif pfunc.__name__ == "p_rel":

        def st_val_map(x):  # before log
            return {numpy.inf: "inf", -numpy.inf: "-inf", numpy.nan: 1}.get(x, x)

        def st_z_map(x):  # after log
            return {numpy.inf: 12, -numpy.inf: -12, numpy.nan: 0}.get(x, x)

    elif pfunc.__name__ == "p_diff":

        def st_val_map(x):
            return {numpy.inf: "inf", -numpy.inf: "-inf", numpy.nan: 0}.get(x, x)

        def st_z_map(x):
            return {numpy.inf: 12, -numpy.inf: -12, numpy.nan: 0}.get(x, x)

    else:
        raise ValueError

    if norm is None:

        def get_znorm(vals):
            return 1

    elif norm == "bylevel":

        def get_znorm(vals):
            return vals.loc[numpy.isfinite(vals)].abs().sum()

    elif norm == "bylevelmax":

        def get_znorm(vals):
            return vals.loc[numpy.isfinite(vals)].abs().max()

    else:
        raise ValueError

    # flip around values if docs are target
    if doc_position=='target':
        values = values.reorder_levels([2, 3, 0, 1])

    def get_sizes(source_level, source_b, target_level):
        if doc_position == "source":
            return source_blocks[source_level].eq(source_b).sum()
        elif doc_position == "target":
            return target_blocks.groupby(target_level).size()

    # apply pfunc between local values and total (top hierarchy) values
    def apply_pfunc():
        if pfunc is not None:
            if doc_position=='source':
                (b_top, g_top), =  values[(max(source_levels),)].groupby(level=0)
                vals_all = g_top[(b_top, target_level)].loc[target_bindex]
                sizes = get_sizes(max(source_levels), b_top, None)
                return pfunc(vals, vals_all / sizes)
            elif doc_position=='target':
                val_all, = values[(source_level, source_hb[-1], max(target_levels))]
                size, = get_sizes(None, None, max(target_levels))
                return pfunc(vals, val_all / size)
            else:
                return vals

    # "st_.*"[source_map_index][target_map_index]: values
    st_val, st_z = [], []
    total_blocks = sum(source_blocks.groupby(l).ngroups for l in source_levels)
    prog = tqdm(desc="Linking", total=total_blocks)
    for source_level in list(reversed(source_levels)):
        source_hbindex = get_sorted_hierarchical_block_index(
            source_blocks, source_levels, source_level
        )
        for source_hb in source_hbindex:
            st_val.append([])
            st_z.append([])
            for target_level in reversed(target_levels):
                target_hbindex = get_sorted_hierarchical_block_index(
                    target_blocks, target_levels, target_level
                )
                target_bindex = [tb[-1] for tb in target_hbindex]
                vals = values.loc[(source_level, source_hb[-1], target_level)].loc[target_bindex]

                # fraction of domain
                vals = vals / get_sizes(source_level, source_hb[-1], target_level)

                # pfunc with baseline
                vals = apply_pfunc()

                # store non normalized data after applying boundary limits
                st_val[-1].extend(vals.map(st_val_map).loc[target_bindex].to_list())

                # bokeh logscale "zero position" bug
                if pfunc.__name__ == "p_rel":
                    vals = vals.apply(numpy.log)

                # store data after normalizing and applying boundary limits
                znorm = get_znorm(vals)
                st_z[-1].extend(
                    (vals / (znorm or 1)).map(st_z_map).loc[target_bindex].to_list()
                )

            prog.update()
    prog.close()

    link_on_source = target_fig.select_one('{}_map_data'.format('ter' if doc_position=='source' else 'doc'))
    source_fig.add_tools(
        bkm.TapTool(
            behavior="select",
            callback=CustomJS(
                args=dict(source=link_on_source, st_val=st_val, st_z=st_z),
                code="""
        index = cb_data.source.selected.indices[0];
        source.data.z = st_z[index]
        source.data.value = st_val[index]
        source.change.emit()
        """,
            ),
        )
    )
