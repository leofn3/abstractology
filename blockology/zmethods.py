# coding: utf-8

# Abstractology - Study of the organisation and evolution of a corpus
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Project:
# <https://en.wikiversity.org/wiki/The_dynamics_and_social_organization_of
#  _innovation_in_the_field_of_oncology>
#
# Reference repository for this file:
# <https://gitlab.com/solstag/abstractology>
#
# Contributions are welcome, get in touch with the author(s).

import pandas
from os import path
from collections import OrderedDict, Counter, defaultdict
from functools import partial
from numbers import Number
from tqdm import tqdm

from .. import ioio


class Zmethods:
    """
    Methods to be used with plot_block_level_map.

    'zmethods' should be defined as:

    def example(self, blocks, level, index)

    and should return a pandas.Series of a scalar dtype and indexed by 'index'.
    """

    def __init__():
        return

    def count(self, blocks, level, index):
        count = blocks.groupby(blocks.loc[:, level]).size()
        count = count.reindex(index)
        count = count.where(count.notnull(), 0)
        return count

    def density(self, blocks, level, index):
        count = blocks.groupby(blocks.loc[:, level]).size()
        count = count.reindex(index)
        count = count.where(count.notnull(), 0)
        dens = count / count.sum()
        return dens

    def t_doc_density(self, tblocks, tlevel, index):
        term_groups = tblocks[tlevel].groupby(tblocks[tlevel])
        count = pandas.Series(index=tblocks[tlevel].unique())
        for n, g in tqdm(term_groups):
            s = set()
            for t in g.index:
                s.update(self.ter_documents[t])
            count.loc[n] = len(self.data.index.intersection(s))
        count = count.reindex(index)
        count = count.where(count.notnull(), 0)
        value = count / count.sum()
        return value

    def e_doc_density(self, eblocks, elevel, index):
        ext_groups = eblocks[elevel].groupby(eblocks[elevel])
        count = pandas.Series(index=eblocks[elevel].unique())
        for n, g in tqdm(ext_groups):
            s = set()
            for e in g.index:
                s.update(self.ext_documents[e])
            count.loc[n] = len(self.data.index.intersection(s))
        count = count.reindex(index)
        count = count.where(count.notnull(), 0)
        value = count / count.sum()
        return value

    # Auxiliary methods, used in zmethods or to generate them

    def density_pair_gen(idx0, idx1, func):
        def density_pair(self, blocks, level, index):
            count0 = blocks.loc[idx0].groupby(blocks[level]).size()
            count0 = count0.reindex(index)
            count0 = count0.where(count0.notnull(), 0)
            dens0 = count0 / count0.sum()
            count1 = blocks.loc[idx1].groupby(blocks[level]).size()
            count1 = count1.reindex(index)
            count1 = count1.where(count1.notnull(), 0)
            dens1 = count1 / count1.sum()
            value = func(dens0, dens1)
            return value.where(value.notnull(), 1)  # 0/0 => 1

        density_pair.__name__ = "density_{}_{}_{}".format(
            func.__name__, idx0.name, idx1.name
        )
        return density_pair

    def x_doc_density_pair_gen(idx0, idx1, func, btype):
        def x_doc_density_pair(self, xblocks, xlevel, index):
            x_documents = getattr(self, "{}_documents".format(btype))
            x_groups = xblocks[xlevel].groupby(xblocks[xlevel])
            count0 = pandas.Series(index=xblocks[xlevel].unique())
            count1 = pandas.Series(index=xblocks[xlevel].unique())
            index0 = self.data.index.intersection(idx0)
            index1 = self.data.index.intersection(idx1)
            for n, g in tqdm(x_groups):
                s = set()
                for x in g.index:
                    s.update(x_documents[x])
                count0.loc[n] = len(index0.intersection(s))
                count1.loc[n] = len(index1.intersection(s))
            count0 = count0.reindex(index)
            count0 = count0.where(count0.notnull(), 0)
            count1 = count1.reindex(index)
            count1 = count1.where(count1.notnull(), 0)
            value = func(count0 / count0.sum(), count1 / count1.sum())
            return value.where(value.notnull(), 1)

        x_doc_density_pair.__name__ = "{}_doc_density_{}_{}_{}".format(
            btype, func.__name__, idx0.name, idx1.name
        )
        return x_doc_density_pair

    def load_cross_doc_count_all(self, btype, store=True, from_file=True):
        """
        Pairs every domain from every level with every cross block from every
        level (topics or extended blocks) and counts the number of documents
        connected to each cross block.

        Parameters
        ----------
        self: an instance of `Graphology`
        btype: str
            The block type to cross. Either 'ter' or 'ext'.
        store: bool
            Store the result to a file.
        from_file: bool
            Attempt to read from a stored result.

        Result
        ------
        Sets self.'values_{btype}_doc_count_all', to a pandas.Series with MultiIndex

        (domain level, domain, cross level, cross block)
        """
        blocks = self.blocks
        xblocks, xblocks_levels, _ = self.get_blocks_levels_sample(btype)
        x_documents = getattr(self, "{}_documents".format(btype))
        fname = "values_{}_doc_count_all".format(btype)
        fdir = (
            self.blocks_dir
            if btype == "ter"
            else self.chained_dir
            if btype == "ext"
            else None
        )
        if from_file:

            try:
                values = ioio.load(fname, fdir=fdir, format="json",)
                values = pandas.Series(
                    index=pandas.MultiIndex.from_tuples(values["index"]),
                    data=(x for x in values["data"]),
                )
                setattr(self, fname, values)
                print("Loaded from file")
                return
            except FileNotFoundError:
                pass

        keys, vals = [], []
        for xlevel in tqdm(xblocks_levels, desc="Cross level"):
            x_groups = xblocks[xlevel].groupby(xblocks[xlevel])
            for xb, xg in tqdm(x_groups, desc="Term block"):
                xb_docs = set()
                for x in xg.index:
                    xb_docs.update(x_documents[x])
                for level in tqdm(self.blocks_levels, desc="Doc level"):
                    doc_groups = blocks[level].groupby(blocks[level])
                    for b, g in doc_groups:
                        keys.append((level, b, xlevel, xb))
                        vals.append(len(g.index.intersection(xb_docs)))

        values = pandas.Series(vals, index=pandas.MultiIndex.from_tuples(keys))

        setattr(self, fname, values)
        if store:
            print("Storing values")
            ioio.store_pandas(
                values, fname, fdir=fdir, format="json",
            )

    def p_diff(a, b):
        return a - b

    def p_rel(a, b):
        return a / b
