# coding: utf-8

# Abstractology - Study of the organisation and evolution of a corpus
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Project:
# <https://en.wikiversity.org/wiki/The_dynamics_and_social_organization_of
#  _innovation_in_the_field_of_oncology>
#
# Reference repository for this file:
# <https://gitlab.com/solstag/abstractology>
#
# Contributions are welcome, get in touch with the author(s).

import matplotlib
matplotlib.use('Agg')

import gzip, json, os, pickle, re
import numpy, pandas
from os import path
from functools import lru_cache
from collections import OrderedDict, Counter
from copy import deepcopy

from gensim.models import phrases
from itertools import combinations
from matplotlib import pyplot as plt, colors
from matplotlib.backends.backend_pdf import PdfPages
from scipy.sparse import coo_matrix
from tqdm import tqdm

from . import makep, _setp_dir, property_getfuncdir, naming, ioio
from . parsers import CorpusParsers
from . util.wos_lam import WosLam

class Corporalogy(CorpusParsers):
    """
    Create, save and modify core data from corpora of text and metadata.
    """
    def __init__(self, analysis_dir=None, loaded=None, storage_dir=None,
                 name_extra=None, text_source=None, column=None, time=None,
                 labels={}, load_data=True):
        """
        Initialize from stored values if found, then apply arguments
        """
        wargs = locals()
        defaults = dict(storage_dir='auto_abstractology', name_extra='',
                        text_source=None, column=None, time=None)

        self.data = pandas.DataFrame()
        self.odata = pandas.DataFrame()
        self.loaded = {'data': []}
        self._labels = {'id':None, 'title':None, 'authors':None,
                       'affiliations':None, 'source':None}

        to_load = {'data': [], 'properties': {}}
        if loaded is not None:
            to_load = ioio.load(loaded, format='json')
        elif analysis_dir is not None:
            if path.isfile(path.join(analysis_dir, 'loaded')):
                to_load = ioio.load('loaded', analysis_dir, format='json')
        self.config_fpath = loaded

        self._load_properties(wargs, to_load, defaults)

        if analysis_dir is not None:
            self.analysis_dir = analysis_dir
        elif 'analysis_dir' in to_load:
            self.analysis_dir = to_load.pop('analysis_dir')
        else:
            self.analysis_dir = path.join(self.storage_dir, 'reports')

        self.labels.update(to_load['properties'].pop('labels', {}))
        self.labels.update(labels)

        if load_data:
            for data_source in to_load.pop('data'):
                self.load_data(data_source)

        self._to_load = to_load

    ext_data = '.pickle'

    def register_loaded(self, fpath=None):
        if fpath is None:
            fpath = self.config_fpath
        if fpath is None:
            fpath = path.join(self.analysis_dir, 'loaded')
        properties = dict( (p, getattr(self, p))
                             for p in dir(type(self))
                             if (type(getattr(type(self), p)) is property)
                                and hasattr(self, '_'+p) )
        loaded = deepcopy(self._to_load)
        loaded.update(self.loaded)
        loaded.setdefault('properties', {}).update(properties)
        ioio.store(loaded, fpath,
                   format='json', compress=False,
                   formatter_args={'indent':2, 'sort_keys':True})

    ####################
    # Class properties #
    ####################

    def _load_properties(self, wargs, to_load, defaults):
        to_load_p = to_load.get('properties', {})
        for p in defaults:
            if wargs.get(p, None) is not None:
                setattr(self, p, wargs[p])
            elif p in to_load_p:
                setattr(self, p, to_load_p.pop(p))
            else:
                setattr(self, p, defaults[p])

    def _set_column(self, column):
        if column in self.data.columns or self.data.empty:
            if hasattr(self, 'ter_documents'):
                del self.ter_documents
            self.cache_clear()
            self._column = column
        else:
            raise KeyError('`column` must be a valid key in the data.')

    text_source = property(*makep('text_source'))
    column = property(makep('column')[0], _set_column)
    time = property(*makep('time'))
    labels = property(makep('labels')[0])
    name_extra = property(*makep('name_extra'))

    analysis_dir = property(*makep('analysis_dir', setp=_setp_dir))
    storage_dir = property(*makep('storage_dir', setp=_setp_dir))
    data_store = property_getfuncdir(
        lambda self: path.join(self.storage_dir, 'data') )
    data_name = property(lambda self: '|'.join(self.loaded['data']))
    data_dir = property_getfuncdir(
        lambda self: path.join(self.data_store, self.data_name))
    data_adir = property_getfuncdir(
        lambda self: path.join(self.analysis_dir, self.data_name))

    ################
    # I/O methods #
    ################

    def get_datasets(self):
        return [fname for fname in os.listdir(self.data_store)
                      if naming.splitext(fname)[1] == self.ext_data]

    def store_data(self, columns=None, overwrite=False):
        """
        Data is stored under `self.data_store` inside a directory carrying the
        same name as the file to be saved. The file name itelf is derived from
        the value of `self.loaded['data']`.

        If you want to store the data somewhere else, use `self.store` instead.
        """
        data = self.data if columns is None else self.data[columns]
        if ( not overwrite
             and path.exists(path.join(self.data_dir, self.data_name)) ):
            raise ValueError('File {} already exists in {}. If you are sure, '
                'pass `overwrite=True`'.format(self.data_name, self.data_dir) )
        ioio.store_pandas(data, self.data_name, self.data_dir, gzipped=True)
        print('Stored: {} in {}'.format(self.data_name, self.data_dir))

    ################
    # Data methods #
    ################

    def load_data(self, source, parser='storage', load=True, parser_args={}):
        """
        Loads data into `self.data`.

        Parameters
        ----------
        source: str
            File to be parsed.
        parser: str
            Format of source, any of `CorpusParsers.get_{format}`.
        load: bool
            Try loading a previously parsed file; only parse if not found.
        parser_args: dict
            Some parsers can take arguments
        """
        source = path.normpath(source)
        nargs = [('extra', self.name_extra)] if self.name_extra else []
        name = naming.gen(path.basename(source), nargs, self.ext_data)
        data, loaded = None, None

        if parser in ('storage', 'pickle'):
            fdir = (path.join(self.data_store, source) if parser=='storage' else
                    path.dirname(source))
            data = pandas.DataFrame(ioio.load(path.basename(source), fdir=fdir))
            print('Data loaded: {}'.format(source))
            loaded = source

        elif parser in ('csv', 'csvdir', 'cortext', 'esmo', 'getpapers',
                        'getpapersdir', 'pubmed'):
            if load:
                try:
                    data = ioio.load_pandas(name,
                                            path.join(self.data_store, name))
                    loaded = name
                    print('Existing parse found, corpus autoloaded: {}'
                          .format(name))
                except FileNotFoundError:
                    pass

            if data is None:
                data = getattr(self, 'get_'+parser)(source, **parser_args)
                loaded = name

        else:
            raise AttributeError('Parser not found: {}'.format(parser))

        if not self.odata.empty:
            data.index = data.index + (1 + self.odata.index.max())
        self.odata = self.odata.append(data, verify_integrity=True)
        self.set_sample(None)
        if loaded is not None:
            self.loaded['data'].append(loaded)

    def process_corpus(self, bigrams=True):
        """
        Splits and filters raw text data it into a tokenized list of lists.
        Optionally infers and joins bigrams using `gensim.phrases.Phraser`.

        Parameters
        ----------
        bigrams: whether to join bigrams
        """
        if bigrams: # TODO do this properly with _set_column ?
            colname = '_'.join([self.column, 'ng2'])
            if hasattr(self, 'ter_documents'):
                del self.ter_documents
            self.cache_clear()
            self._column = colname

        if self.column in self.data:
            raise ValueError(
                'Key `{}` already present in data.'.format(self.column))

        re_sentence = re.compile('[\.!?][\s$]')
        re_term = re.compile('[#|$%/\\\=:;,?!"\'”“\(\)\[\]\{\}\<\>\.]+')

        def texttosentences(doc):
            return re_sentence.split(doc) if isinstance(doc, str) else []

        def sentencetowords(sentence):
            re_alpha = re.compile('[^\W\d_]')
            wordlist = [ word
                         for inter in sentence.casefold().split()
                         for dirty in re_term.split(inter)
                         for word in ( dirty.strip('.-_'),)
                           if len(word)>1 and re_alpha.search(word) ]
            return wordlist


        print('Building tokens from `{}`'.format(self.text_source))
        tokens = [
            tuple( tuple(wordlist)
                   for sentence in texttosentences(text)
                     for wordlist in (sentencetowords(sentence),)
                       if wordlist )
            for text in self.data[self.text_source] ]

        if bigrams:
            # Infer and join bigrams so ['são', 'paulo'] becomes ['são_paulo']
            print('Consolidating bigrams')
            bigram = phrases.Phraser(
                phrases.Phrases(s
                                for d in tokens
                                for s in d) )
            tokens = [tuple( tuple(bigram[s]) for s in d )
                                       for d in tokens ]

        self.data[self.column] = tokens

        empty = ~self.data[self.column].astype(bool)
        duplicated = self.data[self.column].duplicated().sum()
        print('`{}` created with {} empty and {} duplicate entries'.format(
            self.column, empty.sum(), duplicated.sum()))


    def transform_corpus(self, trans=set(), remove_dupes=set(), inplace=False):
        """
        Parameters
        ----------
        trans : a set of transformations to apply tokens. See below.
        remove_dupes: a set, may contain
          - `None`: remove all rows with empty values in `self.column`
          - `self.text_source`: keep only first row when text is duplicated
          - `self.column`: keep only the first row when tokens are duplicated
          - any transform: same as above but under given transform

        Transformation
        --------------
        New columns will be named in the form: `{self.column}_{transform}`

        Available transforms are:
        'set' - words appear once per document
        'red' - vocabulary is reduced to 20% highest graded words
        'redset' - apply red to vocabulary and set to document
        'propo' - each document is cut to its 20% highest graded words
        'proposet' - apply set to document then propo

        Returns
        -------
        A copy of the data with added columns and removed rows
        """
        trans = set([trans]) if isinstance(trans, str) else set(trans)
        print('Transforming corpus into {}'.format(trans))
        data = self.data if inplace else self.data.copy()

        def cn(x):
            "Get the column name for each transformation"
            return '_'.join([self.column, x])

        # Apply transformations
        if 'set' in trans:
            data[cn('set')] = [(tuple(set(w for s in d for w in s)),)
                               for d in data[self.column] ]

        if trans.intersection({'red', 'redset','propo', 'proposet'}):
            grade, wfreq, dfreq = self.get_graded_vocab(self.column)

        if trans.intersection({'red', 'redset'}):
            vocab = filter(lambda x: dfreq[x]>( len(data)/1000), grade.keys())
            vocab = sorted(vocab, key=lambda x: grade[x])
            mingrade = grade[vocab[-int(len(vocab)/5)]]
            vocab = set(vocab[-int(len(vocab)/5):])
            print('Reduced {} words with dfreq {} and grade {} retaining {} words' \
                    .format( len(grade.keys()), len(data)/1000,
                             mingrade, len(vocab)))
        if 'red' in trans:
            data[cn('reduced')] = [tuple( tuple(w for w in s if w in vocab)
                                          for s in d )
                                   for d in data[self.column] ]
        if 'redset' in trans:
            data[cn('redset')] = [(tuple(set( w for s in d
                                              for w in s if w in vocab )),)
                                  for d in data[self.column] ]

        if trans.intersection({'propo', 'proposet'}):
            print('Propo!')
            def get_propo(d, setit=False):
                # wfreq[w]>=5 matches word2vec default vocab cut
                s = pandas.Series( w for s in d for w in s if dfreq[w]>=5) # w or d ?
                if setit:
                    s = s[~s.duplicated()]
                # s = s.sample(frac=1) # if comntd: deterministic but prefer early
                idx = s.apply(lambda x: -grade[x]).sort_values().index
                idx = idx[:int(len(s)/5)]
                s = s.loc[idx].sort_index()
                return (tuple(s),)
        if 'propo' in trans:
            data[cn('propo')] = data[self.column].transform(get_propo)
        if 'proposet' in trans:
            data[cn('proposet')] = data[self.column].transform(get_propo,
                                                               setit=True)

        # Find empty and duplicate entries, opt remove them and reset index
        remove = {}
        remove[None] = ~data[self.column].astype(bool)
        remove[self.text_source] = data[self.text_source].duplicated()
        remove[self.column] = data[self.column].duplicated()
        for tr in trans:
            remove[tr] = data[cn(tr)].duplicated()
        print('Empty (None) or duplicated:',
              *[[k, v.sum()] for k, v in remove.items()])
        if remove_dupes:
            cis = (None, self.text_source, self.column)
            to_remove = [remove[x if x in cis else cn(x)] for x in remove_dupes]
            b_remove = numpy.add.reduce(to_remove).astype(bool)
            data = data[~b_remove]
            if inplace:
                self.data = data
            data.reset_index(drop=True, inplace=True)
            print('Eliminated {} entries'.format(b_remove.sum()))

        return data

    def itersentences(self, mode, docs=None):
        if docs is None: docs = self.data[self.column]
        if mode == 'document':
            return ( sum(d, ()) for d in docs )
        elif mode == 'sentences':
            return ( s for d in docs for s in d )

    def indexsentences(self, mode, docs=None):
        if docs is None: docs = self.data[self.column]
        if mode == 'document':
            return docs.index
        elif mode == 'sentences':
            return ( i for i, d in docs.iteritems() for s in d )

    def shuffled_data(self, data=None, reshuffle=False):
        if data is None:
            data = self.data
        if (self.shuffled_index is None) or reshuffle:
            self.shuffled_index = list(self.data.index)
            numpy.random.shuffle(self.shuffled_index)
        return data.loc[self.shuffled_index]

    def balance_groups(self, grouped_data, balance):
        """
        Balance the size of groups of documents according to a chosen method
        """
        if balance == 'nobalance':
            for g, ga in grouped_data:
                yield g, ga
        elif balance == 'randomfill':
            # Fill them up to match the largest group
            maxsize = max( grouped_data.size() )
            for group, gdata in grouped_data:
                size = len(gdata)
                factor = int(maxsize/size)
                rest = maxsize - size*factor
                yield group, \
                      pandas.concat( [gdata for i in range(factor)] + \
                                     [ gdata[:rest] ])
        elif balance == 'randomsample':
            # Cut them to match the smallest group
            minsize = min( grouped_data.size() )
            for group, gdata in grouped_data:
                yield group, gdata[:minsize]
        elif balance == 'demisample':
            # Cut them to match half the smallest group
            deminsize = int( min( grouped_data.size() ) / 2 )
            for group, gdata in grouped_data:
                yield group, gdata[:deminsize]
        elif balance == 'antidemisample':
            # The complement of demiample
            minsize = min( grouped_data.size() )
            deminsize = int( min( grouped_data.size() ) / 2 )
            for group, gdata in grouped_data:
                yield group, gdata[deminsize:minsize]
        else:
            raise Exception('Balance method not found: ' + balance)

    def balanced_data(self, groupby, balance):
        return pandas.concat(
            gdata for group, gdata in
                self.balance_groups(
                    self.shuffled_data().groupby(groupby),
                    balance ) ).sort_index()

    def load_wos_citations(self, user=None, password=None, pmids=None):
        if pmids is None: pmids = self.data.pmid
        try:
            wos_citations = ioio.load_pandas('wos_citations'+self.ext_data,
                                             self.data_dir)
        except FileNotFoundError:
            wos_citations = pandas.Series()
        new_pmids = pmids[~pmids.isin(wos_citations.index)]
        print('Found {} stored, downloading {} new citation counts'.format(
                  pmids.size-new_pmids.size, new_pmids.size))
        if not new_pmids.empty:
            try:
                new_citations = pandas.Series()
                wos = WosLam(user, password)
                for pmid, cited in zip(new_pmids, wos.pmid_to_timescited(new_pmids)):
                    new_citations.loc[pmid] = 0 if cited is None else int(cited)
            finally:
                wos_citations = wos_citations.append(new_citations,
                                  verify_integrity=True)
                wos_citations.name='WoS citations'
                ioio.store_pandas(wos_citations, 'wos_citations'+self.ext_data,
                                  self.data_dir)
        self.wos_citations = wos_citations.loc[self.data.pmid]
        self.wos_citations.index = self.data.index
        print('wos_citations loaded, remember to self.norm_by_mean( , normby="year")')

    def get_data(self, fields=None, search={}):
        """
        fields: list of self.data fields to return
        search: dict of 'field'->'regex' to filter data
        """
        if not search:
            return self.data.loc[:, fields] if fields else self.data
        def picker(s):
            if type(search[s.name]) is str:
                return s.astype(str).apply(re.compile(search[s.name]).search).astype(bool)
            elif type(search[s.name]) in (int, float):
                return s==search[s.name]
            else:
                return s.apply(search[s.name]).astype(bool)
        picks = self.data.loc[:, search.keys()].apply( picker ).all(axis=1)
        return self.data.loc[picks, fields] if fields else self.data.loc[picks]

    def get_vocab(self):
        if hasattr(self, 'ter_documents'):
            return set(self.ter_documents.keys())
        else:
            return set(w for d in self.data[self.column] for s in d for w in s)

    def filter_words(self, func, column=None):
        if column is None: column = self.column
        return self.data[self.column].transform(
            lambda d: tuple( tuple(w for w in s if func(w)) for s in d ) )

    def get_col_type(self, column):
        if self.data[column].dtype.kind in 'Mm': return 'string'
        if self.data[column].dtype.kind in 'ifu': return 'int'

    def clear_data(self):
        self.data, self.odata = pandas.DataFrame(), pandas.DataFrame()
        self.loaded['data'] = []

    ###########################
    # Data derivation methods #
    ###########################

    @lru_cache(maxsize=None)
    def get_graded_vocab_cached(self, column, sampled):
        return self.get_graded_vocab(column, sampled)

    def get_graded_vocab(self, column=None, sampled=True):
        """
        Ranks a vocabulary by how concentrated a word appears, given its
        overall frequency, regarding the actual and expected number of
        documents it appears in.
        * corpus: should be a list of documents, which are lists of sentences,
        which are lists of words
        """
        data = self.data if sampled else self.odata
        corpus = data[self.column if column is None else column]
        ndocs = len(corpus)
        docs = corpus.apply(lambda x: sum(x, ()))
        vocab = self.get_vocab()
        wfreq = Counter(w for d in docs for w in d)
        dfreq = Counter(w for d in docs for w in set(d))
        def expected_spread(w):
            return ndocs * ( 1 - ((ndocs-1)/ndocs)**wfreq[w] )
        def concentration(w):
            return expected_spread(w) / dfreq[w]
        graded_vocab = dict( (w, concentration(w)) for w in vocab)
        return graded_vocab, wfreq, dfreq

    def load_dtm(self, store=False, from_file=True):
        """
        Calculates the boolean Document X Term matrix.

        See also: load_dtm_conc (same function but concurrent)
        """
        fname = naming.gen('document_term_matrix', {'column': self.column},
                           '.pickle')
        if from_file:
            try:
                self.dtm = ioio.load(fname, self.data_dir)
                return
            except FileNotFoundError:
                pass

        index = self.data.index
        vocab = list(self.get_vocab())
        data, rows, cols = [], [], []
        docs = self.data[self.column].apply(lambda x: set(sum(x, ()))) #bool:set

        from progress.bar import Bar
        Bar.etam = property(lambda self: self.eta//60)
        bar = Bar('Processing', max=len(index),
                  suffix='%(percent).1f%% - %(elapsed)ds - %(etam)dm ' +
                  '(%(index)d/%(max)d)')

        for row, ind in enumerate(index):
            doc = docs[ind]
            for col, term in enumerate(vocab):
                if term in doc: # bool:in
                    data.append(True) # bool:in
                    rows.append(row)
                    cols.append(col)
            bar.next()
        bar.finish()

        print('Instantiating document term matrix')
        matrix = coo_matrix((data, (rows, cols)), dtype=bool)
        dtm = pandas.SparseDataFrame(data=matrix, index=index, columns=vocab,
                                     default_fill_value=False)
        self.dtm = dtm
        if store:
            print('Storing document term matrix')
            ioio.store(self.dtm, fname, self.data_dir)

    def load_dtm_conc(self, store=False, from_file=True):
        """
        Calculates the boolean Document X Term matrix using concurrent processes.
        """
        fname = naming.gen('document_term_matrix', {'column': self.column},
                           '.pickle')
        if from_file:
            try:
                self.dtm = ioio.load(fname, self.data_dir)
                print('Loaded from file')
                return
            except FileNotFoundError:
                pass

        from multiprocessing.managers import BaseManager
        from concurrent.futures import ProcessPoolExecutor

        index = self.data.index
        vocab = list(self.get_vocab())
        docs = self.data[self.column].apply(lambda x: set(sum(x, ()))) #bool:set

        class MyManager(BaseManager): pass
        MyManager.register('tqdm', tqdm)
        with MyManager() as manager:
            bar = manager.tqdm(total=len(index))
            with ProcessPoolExecutor() as executor:
                step = 1 + len(index)//executor._max_workers
                indexes = [index[n*step:(n+1)*step]
                             for n in range(executor._max_workers)]
                result = []
                for n, idx in enumerate(indexes):
                    args = [vocab, docs, idx, n*step, bar]
                    result.append( executor.submit(process_slice, *args) )
            bar.close()
        print('Instantiating document term matrix')
        data, rows, cols = [sum(x, [])
                              for x in zip(*[r.result() for r in result])]
        matrix = coo_matrix((data, (rows, cols)),
                            shape=(len(index), len(vocab)), dtype=bool)
        dtm = pandas.SparseDataFrame(data=matrix, index=index, columns=vocab,
                                     default_fill_value=False)
        self.dtm = dtm
        if store:
            print('Storing document term matrix')
            ioio.store(self.dtm, fname, self.data_dir)

    def load_ter_documents(self):
        docs = self.data[self.column].apply(lambda x: set(sum(x, ())))
        d = dict()
        for doci, doc in tqdm(docs.items(), total=len(docs), desc='Doc'):
            for w in doc:
                d.setdefault(w, set()).add(doci)
        self.ter_documents = d

    ####################
    # Analysis methods #
    ####################

    def serify(self, axis):
        return self.data[axis] if type(axis) is str else axis

    def samplify(self, sample, source):
        if sample is None:
            return source.index
        elif type(sample) is int:
            index = source.sample(sample).index
        elif type(sample) in (pandas.Series, pandas.DataFrame):
            index = source.index.intersection(sample.index)
        else:
            index = source.index.intersection(sample)
        return index

    def set_sample(self, sample):
        self.data = self.odata.loc[self.samplify(sample, self.odata)]

    def norm_by_mean(self, s, normby):
        normby = self.serify(normby)
        s_avgby = s.groupby(normby).mean()
        return s / normby.apply( lambda x: s_avgby.loc[x])

    def plot_hist(self, s, by=None, scale='linear', bins=10):
        s, by = map( self.serify, (s, by) )
        plt.figure(figsize=(17,10))
        if bins == 'cat':
            bins = len(s.unique())
        s.hist( by=by, bins=bins )
        plt.title( '{} histogram'.format(s.name.capitalize()) + \
                   ('by {}'.fomat(by.name) if by is not None else '') )
        plt.savefig( path.join( self.data_adir, 'hist-{}{}.pdf'.format(
                s.name, ('-by-{}' if by is not None else '') ) ) )
        plt.close()

    def plot_cor_s(self, xaxis, yaxis, scale=('linear', 'linear'), xlim=None, ax=None):
        xscale, yscale = (scale, scale) if type(scale)==str else scale
        if ax == None:
            ax = plt
            ax.figure(figsize=(17, 10))
            ax.title('{} X {}'.format(xaxis.name, yaxis.name))
        ax.scatter(xaxis, yaxis)
        if xscale=='log':
            bins = numpy.logspace( numpy.log10(xaxis.min()+1),
                                   numpy.log10(xaxis.max()+1.2) ) - 1.1
            xaxis = pandas.cut(xaxis, bins, labels=bins[1:])
            if ax == plt:
                ax.xscale(xscale)
            else:
                ax.set_xscale(xscale)
        yavg = yaxis.groupby(xaxis).mean()
        ax.plot(numpy.array(yavg.index), yavg, color='red', linewidth=2)
        #ax.axhline()
        if ax == plt:
            ax.yscale(yscale)
        else:
            ax.set_yscale(yscale)
        if xlim:
            ax.xlim(**xlim)
        if ax == plt:
            ax.savefig( path.join( self.data_adir,
                                   'cor-{}-x-{}-{}.pdf'.format(
                                       xaxis.name, yaxis.name,
                                       ''.join(scale) ) ) )
            input('Press enter to close the plot.')
            ax.close()

    def plot_cor(self, xaxis, yaxis, groupby=None, scale=('linear','linear'),
                 xlim=None):
        xaxis, yaxis, groupby = map(self.serify, (xaxis, yaxis, groupby))
        if groupby is None:
            return self.plot_cor_s(xaxis, yaxis, scale, xlim)
        grouped = xaxis.groupby( groupby )
        for i, (name, group) in enumerate(grouped):
            ax = plt.subplot(int(len(grouped)/2)+1, 2, i+1)
            ax.set_title(name)
            self.plot_cor_s(group, yaxis.loc[group.index], scale, xlim, ax)
        ax.figure.set_size_inches(17.0, len(grouped)*1.7)
        ax.figure.tight_layout()
        ax.figure.suptitle('{} X {}'.format(xaxis.name, yaxis.name), fontsize=17)
        plt.subplots_adjust(top=0.95)
        plt.savefig( path.join( self.data_adir,
                                'cor-{}-{}-x-{}-{}.pdf'.format(
                                    xaxis.name, groupby.name, yaxis.name,
                                    ''.join(scale) ) ) )
        plt.close()

    def print_cor(self, xaxis, yaxis, groupby=None):
        from scipy.stats import pearsonr, spearmanr
        xaxis, yaxis, groupby = map(self.serify, (xaxis, yaxis, groupby))
        grouped = xaxis.groupby( groupby ) if groupby else (xaxis.name, xaxis)
        for name, group in grouped:
            print('==Pandas==\npearson: {}\nspearman: {}\n'.format(
                    group.corr(yaxis, method='pearson'),
                    pearsonr(group, yaxis) ) )
            print('==Scipy==\npearson: {}\nspearman: {}\n'.format(
                    group.corr(yaxis, method='spearman'),
                    spearmanr(group, yaxis) ) )

    def plot_word_frequency_profile(self, words, groupby=None, level='word'):
        """ * level: can be either word or doc"""
        ax = plt.axes()
        docs = self.data[self.column].apply(lambda x: sum(x, ()))
        what = (lambda x, y: int(y in x)) if level=='doc' else \
               (lambda x, y: x.count(y))
        for word in words:
            ax = docs.apply(what, args=(word,)) \
                   .groupby(self.data[groupby]) \
                   .sum().plot(ax=ax, label=word)
        plt.legend()
        input('Tell me when to stop!')
        plt.close()

    def plot_wordpair_matrix(self, words, functions, funcname, name='', scale='linear',
                             upper=False, diagonal=True):
        fname = path.join(self.data_adir, '{}-matrix{}-{}.pdf'.format(
            funcname, (('-' + name) if name else ''), scale) )
        with PdfPages(fname) as pdf:
            for key, function in functions.items():
                sim = pandas.DataFrame(columns=words, index=words, dtype=numpy.float64)
                for i in sim.index:
                    for j in sim.columns:
                        if not diagonal and sim.index.get_loc(i) == sim.columns.get_loc(j):
                            sim.loc[i, j] = numpy.nan
                        elif upper and sim.index.get_loc(i) < sim.columns.get_loc(j):
                            sim.loc[i, j] = numpy.nan
                        else:
                            sim.loc[i, j] = function(i,j)
                plt.figure(figsize=(10,7))
                norm = colors.LogNorm() if scale=='log' else colors.Normalize()
                plt.pcolormesh(numpy.ma.masked_array(sim.as_matrix(), sim.isnull()),
                               cmap='OrRd', norm=norm) #, vmin=0, vmax=1)
                plt.yticks( numpy.arange(0.5, len(sim.index), 1),
                            sim.index )
                plt.xticks( numpy.arange(0.5, len(sim.columns), 1),
                            sim.columns, rotation=45)
                plt.xlabel('destination')
                plt.ylabel('source')
                plt.xlim(xmax=len(sim.columns))
                plt.ylim(ymax=len(sim.index))
                plt.colorbar()
                plt.title('{} {}'.format(funcname, key))
                pdf.savefig()
                plt.close()

    def plot_wordpair_profile(self, words, functions, funcname, name=''):
        fname = path.join(self.data_adir,
                    funcname+'-prof'+(('-' + name) if name else '')+'.pdf' )
        with PdfPages(fname) as pdf:
            for w1, w2 in combinations(words, 2):
                plt.plot( list(functions),
                          [function(w1, w2) for function in functions.values()] )
                plt.title('{}( {}, {} )'.format(funcname, w1, w2))
                pdf.savefig()
                plt.close()

    def cache_clear(self):
        for m in dir(type(self)):
            m = getattr(type(self), m)
            if hasattr(m, 'cache_clear'):
                m.cache_clear()

def process_slice(vocab, docs, index, istart, bar):
    """
    Used in `load_dtm_conc`.
    """
    data, rows, cols = [], [], []
    for row, ind in enumerate(index, istart):
        doc = docs[ind]
        for col, term in enumerate(vocab):
            if term in doc: # bool:in
                data.append( True )
                rows.append(row)
                cols.append(col)
        bar.update()
    return data, rows, cols

