# coding: utf-8

# Abstractology - Study of the organisation and evolution of a corpus
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Project:
# <https://en.wikiversity.org/wiki/The_dynamics_and_social_organization_of
#  _innovation_in_the_field_of_oncology>
#
# Reference repository for this file:
# <https://gitlab.com/solstag/abstractology>
#
# Contributions are welcome, get in touch with the author(s).

import matplotlib
matplotlib.use('Agg')

import graph_tool.all as gt
import multiprocessing, numpy, os, pandas, pickle
from os import path
from functools import lru_cache
from itertools import combinations
from scipy.spatial.distance import pdist
from matplotlib import pyplot as plt, colors
from matplotlib.backends.backend_pdf import PdfPages
from tqdm import tqdm

from . import clearattrs, ioio, makep, naming, property_getfuncdir
from . corporalogy import Corporalogy
from . scorology import Scorology
from . blockology import Blockology

#import pdb; # pdb.set_trace()

class Graphology(Blockology, Scorology, Corporalogy):
    """
    Build graphs from corpora and find optimal block models.
    """
    def __init__(self,  *args, gdocvname=None, **kwargs):
        """
        Call `super()` to set up the stage then add class specifics.
        """
        super().__init__(*args, **kwargs)

        defaults = dict(gdocvname=None)
        self._load_properties(locals(), self._to_load, defaults)

        self.loaded['graph'] = self._to_load.pop('graph', None)
        self.loaded['blockstate'] = self._to_load.pop('blockstate', None)
        self.loaded['chainedbstate'] = self._to_load.pop('chainedbstate', None)

        self.graph_extended = None

    ext_g = '.gt.xz'
    ext_nbs = '.nbstate'

    gdocvname = property(*makep('gdocvname'))

    graph_name = property(lambda self: self.loaded['graph'])
    graph_dir = property_getfuncdir(
        lambda self: path.join(self.data_dir, self.graph_name) )
    graph_adir = property_getfuncdir(
        lambda self: path.join(self.data_adir, self.graph_name) )
    blocks_name = property(lambda self: self.loaded['blockstate'])
    blocks_dir = property_getfuncdir(
        lambda self: path.join(self.graph_dir, self.blocks_name) )
    blocks_adir = property_getfuncdir(
        lambda self: path.join(self.graph_adir, self.blocks_name) )
    chained_name = property(lambda self: self.loaded['chainedbstate'])
    chained_dir = property_getfuncdir(
        lambda self: path.join(self.blocks_dir, self.chained_name) )
    chained_adir = property_getfuncdir(
        lambda self: path.join(self.blocks_adir, self.chained_name) )

    ################
    # Graph methods #
    ################

    def from_corpus_to_docvoc(self, sample=None):
        """
        Builds a bipartite undirected graph of documents connecting to the terms
        they contain.

        Parameters
        ----------
        sample: anything consumable by `samplify`
          use the full vocabulary but only a sample of the documents
        """
        docs = self.data[self.column]
        vocab = {w for d in docs for s in d for w in s}
        docs = docs.loc[self.samplify(sample, docs)]
        print('Vocab size: {}'.format(len(vocab)))
        vocabindex = dict( (w, n) for n, w in enumerate(vocab))

        g = gt.Graph(directed=False)
        g.vp['type'] = g.new_vertex_property('int') # type: 0: document, 1:term
        g.vp['name'] = g.new_vertex_property('string')
        g.add_vertex(len(vocab)+len(docs))
        for w, vi in vocabindex.items():
            g.vp['type'][vi] = 1
            g.vp['name'][vi] = w

        def gen_docvoc_edges():
            for vi, di in enumerate(tqdm(docs.index, desc='Processing docs'),
                                    len(vocab)):
                g.vp['type'][vi] = 0
                g.vp['name'][vi] = str(self.data[self.gdocvname][di]) \
                                   if self.gdocvname else di
                for s in docs[di]:
                    for w in s:
                        if w in vocab:
                            yield (vi, vocabindex[w])
                
        g.add_edge_list(gen_docvoc_edges())
        return g

    def from_corpus_to_convoc(self):
        """
        Builds a bipartite undirected graph of terms connecting to the contexts
        they appear in.

        TODO: perhaps a directed graph where contexts connect back their
        composing terms?

        Parameters
        ----------
        """
        pass

    def create_graph(self, by=None, graphtype='docvoc'):
        """
        Creates one or more graphs of a given type, names it, stores it and
        sets as loaded.

        Parameters
        ----------
        by: name of a column from `self.data` or an equivalent `pandas:Series`
          If provided, group documents by this column and create multiple graphs
        graphtype: `str`
          The type of graph to be built. Available types are: 'docvoc'.

        Returns
        -------
        names: a `list` containing the names of the graphs created
        """
        nameargs = [('gtype', graphtype), ('gcol', self.column)]
        if by is None:
            gby = lambda x: 'all'
            def getname(gname):
                return naming.gen('graph', nameargs, self.ext_g)
        else:
            nby = (by if isinstance(by, str) else
                   by.name if hasattr(by, 'name') else
                   by.__name__)
            assert(nby)
            def getname(gname):
                return naming.gen('graph', nameargs + [('by', (nby, gname))],
                                  self.ext_g)

        names = []
        for gname, group in self.data.groupby(gby):
            print('Generating {} graph for {}'.format(graphtype, gname))
            name = getname(gname)
            fpath = path.join(self.data_dir, name, name)
            if graphtype!='docvoc':
                raise ValueError('Unrecognized graph type')
            try:
                os.makedirs(path.dirname(fpath), exist_ok=False)
                g = self.from_corpus_to_docvoc(sample=group.index)
                g.save(fpath)
            except OSError:
                print('Found existing graph. Will skip creation, test loading.')
                g = gt.load_graph(fpath)
            names.append(name)

        if len(names)==1:
            self.set_graph(names[0])
            print('Graph set to: {}'.format(names[0]))
        return names

    def annotate_graph(self, g, vprops=[], eprops=[]):
        """
        Add properties to the vertices and edges of the graph

        Parameters
        ----------
        g: `graph_tool:Graph`
        vprops: `list`
          Columns from `self.data`
        eprops: `list`
          Columns from `self.data`

        Returns
        -------
        g: the graph, with added properties
        """
        for prop in eprops:
            g.vp[prop] = g.new_vertex_property(self.get_col_type(prop))
        for prop in eprops:
            g.ep[prop] = g.new_edge_property(self.get_col_type(prop))
        for v in g.vertices():
            sel = self.data[self.gdocvname] == g.vp['name'][v]
            di, = self.data.loc[sel, self.gdocvname]
            for prop in vprops:
                g.vp[prop][v] = self.data.loc[di, prop]
            for e in v.out_edges():
                for prop in eprops:
                    g.ep[prop][e] = self.data.loc[di, prop]
        return g

    def extend_graph(self, prop, matcher=None):
        """
        Adds nodes from another dimension (data column) to the graph.
        Used to calculate a chained blockstate.

        Parameters
        ----------
        prop: the dimension whose unique values will form the new nodes
        matcher: `None` or `dict`
         if `None`, edges link new nodes to the data rows that contain
          their value
         if `dict`, it must be a dictionary mapping node names to regular
          expressions that will be applied to the `prop` column and nodes
          linked to matching rows.
        """
        graph_extended = {'prop': prop, 'matcher': matcher}
        if isinstance(prop, str):
            prop = self.data[prop]
        if matcher is None:
            extvnames = prop.unique()
        else:
            extvnames  = matcher.keys()

        g = self.graph
        name2vindex = dict( (g.vp['name'][v], v) for v in g.vertices() )
        vtype = max(g.vp['type'])+1
        edge_list = []
        for extvname in extvnames:
            v = g.add_vertex()
            g.vp['type'][v] = vtype
            g.vp['name'][v] = extvname
            if matcher is None:
                matches = prop[prop==extvname].index
            else:
                matches = prop[prop.str.contains(matcher[extvname],
                                                 na=False)].index
            for docidx in matches:
                doc_vindex = name2vindex[
                    str(self.data.loc[docidx, self.gdocvname])]
                edge_list.append( (doc_vindex, v) )
        g.add_edge_list(edge_list)
        self.graph_extended = graph_extended
        self.graph_extended_vtype = vtype

    def load_ext_documents(self):
        prop = self.graph_extended['prop']
        matcher = self.graph_extended['matcher']
        if isinstance(prop, str):
            prop = self.data[prop]
        exts = prop.unique() if matcher is None else matcher.keys()

        d = {}
        for ext in exts:
            if matcher is None:
                matches = prop[prop==ext].index
            else:
                matches = prop[prop.str.contains(matcher[ext], na=False)].index
            d.setdefault(str(ext), set()).update(matches)

        self.ext_documents = d

    ######################
    # Blockstate methods #
    ######################

    def calc_nested_blockstate(self, g=None, name_args=[], overlap=False,
                               state_args={}):
        """
        Calculate and save a nested blockstate for the graph, using
        `graph_tool.inference.minimize.minimize_nested_blockmodel_dl()`.

        Parameters
        ----------
        g: a `graph_tool.Graph` instance
          If not provided, use the current graph.
        name_args: `list` of 2-tuples
          Extra arguments to add to the blockstate filename.
        overlap: `bool`
          Use the overlapping model.
        state_args: `dict`
          Passed to the downstream function; if has key 'ec', its value is
          turned to `g.ep[value]`.
        """
        for count in range(os.sys.maxsize):
            fname = naming.gen('blockstate',
                               [('step', 'mnbdl')]+name_args+[('run', count)],
                               self.ext_nbs)
            try:
                os.makedirs(path.join(self.graph_dir, fname), exist_ok=False)
                print('Reserving name: {}'.format(fname))
                break
            except OSError:
                pass

        if g is None:
            if not hasattr(self, 'graph'):
                self.load_graph()
            g = self.graph

        store_blockstate_args = {'fname':fname,
                                 'fdir':path.join(self.graph_dir, fname)}
        if 'ec' in state_args:
            layers = True
            store_blockstate_args['ec'] = state_args['ec']
            state_args[ec] = g.ep[ec]
            if 'layers' in state_args:
                store_blockstate_args['layers'] = state_args.get['layers']
        else:
            layers = False
        if 'type' in g.vp:
            assert 'pclabel' not in state_args
            store_blockstate_args['pclabel'] = 'type'
            state_args['pclabel'] = g.vp['type']
            print('Vertex property "type" found, using it as pclabel')

        os.sys.stdout.flush()
        state = gt.minimize_nested_blockmodel_dl(g, overlap=overlap,
          layers=layers, state_args=state_args)

        # ioio.store(state, fname, path.join(self.graph_dir, fname))
        self.store_blockstate(state=state, **store_blockstate_args)
        print('Saved state: {}'.format(fname))
        self.loaded['blockstate'] = fname
        return fname

    def continuous_map_nested_blockstate(self, ns, io_is_bs=False):
        bs = ns if io_is_bs else ns.get_bs()
        bs = [b.copy() for b in bs]
        for l, b in enumerate(bs):
            m = dict()
            for i, b_i in enumerate(b):
                b[i] = m.setdefault(b_i, len(m))
            if l+1<len(bs):
                b = bs[l+1]
                bc = b.copy()
                for i, j in m.items():
                    bc[j] = b[i]
                bs[l+1] = bc
        return bs if io_is_bs else ns.copy(bs=bs)

    def get_chained_nested_blockstate(self, prop, matcher,
                                      bext='max', bfield=False):
        """
        Calculates a chained nested blockstate for vertices derived from
        values of some property of the documents.

        Only works for NestedBlockState without layers or overlapping.
        """
        def get_bfield(g, frozen, b):
            field = g.new_vp('vector<double>')
            for i, r in enumerate(b):
                if g.vertex(i) in frozen:
                    field[i] = [(0 if j==r else -numpy.inf) for j in range(r+2)]
            return field

        if not hasattr(self, 'state'):
            self.load_blockstate()
            assert self.graph_extended is None
            # get an independent graph, workaround for graph.copy memory blow
            self.load_graph()
            self.extend_graph(prop, matcher)
        elif self.set_graph(self.graph_name, {'prop':prop, 'matcher':matcher}):
            self.load_graph()
        g = self.graph

        N = self.state.g.num_vertices()
        bs = [list(b) for b in self.state.get_bs()] # effectively copies `b`s
        C = g.num_vertices() - N
        state_args = {}

        if 'type' in g.vp:
            print('Vertex property "type" found, using it as pclabel')
            state_args['pclabel']= g.vp['type']

        print('Extending bs to match new graph')
        for i, b in enumerate(bs):
            if len(bs)>2:
                if i==len(bs)-1: # top state is zero
                    b.append(0); continue
                if i==len(bs)-2: # previous to top is constant
                    b.extend( [max(b) + 1] * C); continue
            elif len(bs)==2:
                if i==len(bs)-1: # top state is constant
                    b.extend( [max(b) + 1] * C); continue
            if bext=='max':
                b.extend(range(max(b) + 1, max(b) + 1 + C))
            elif bext=='min':
                b.extend( [max(b) + 1] * C); continue
            else:
                raise ValueError('Unknown value for `bext`.')

        bs = list(map(numpy.array, bs))

        if bfield:
            print('Creating bfield for the base state')
            lfrozen = [{v for v in g.vertices() if int(v) < N}]
            state_args['bfield'] = get_bfield(g, lfrozen[0], bs[0])

        print('Instantiating NestedBlockState')
        ns = gt.NestedBlockState(g, bs, sampling=True, **state_args)

        # propagate bfield over all levels
        if bfield:
            for l, (s_pre, s_cur) in enumerate(zip(ns.levels, ns.levels[1:])):
                print('Add bfield to level {}/{}'.format(l+1, len(ns.levels)-1))
                lfrozen.append( {s_cur.g.vertex(s_pre.b[v])
                                   for v in s_pre.g.vertices()
                                   if v in lfrozen[-1] } )
                field = get_bfield(s_cur.g, lfrozen[-1], s_cur.b)
                for i, _ in enumerate(s_cur.bfield):
                    s_cur.bfield[i] = field[i]

        print('State created.')

        return ns

    def get_extended_vlist(self, g):
        return [ v for v in g.vertices()
                   if g.vp['type'][v] == self.graph_extended_vtype ]

    def step_chained_nested_blockstate(self, ns, runs=10, steps=2):
        """
        Executes an `steps` steps, `runs` runs optimization of a blockstate.

        A run is a regular MCMC equilibration of a state, keeping track of the
        lowest entropy state discovered.
        A step consists of `runs` runs, all starting from the same state, and
        returns the lowest entropy state over all runs.
        The first step starts from `ns`, subsequent steps start from the state
        returned by the previous one.
        The best state overall gets returned.

        Parameters
        ----------
        ns: gt.NestedBlockState
        runs: int
        steps: int

        Returns
        -------
        bs: list of np.ndarray
            The blockstate of lowest entropy over all discovered.
        """
        vlist = self.get_extended_vlist(ns.g)
        def step(start_bs, desc):
            def cb_run(ns):
                # for efficiency do not consider variations from non continuity
                cur_ent = ns.entropy()
                if cur_ent < best_ent[-1]:
                    best_bs[-1] = [b.copy() for b in ns.get_bs()]
                    best_ent[-1] = cur_ent
                return None

            best_bs, best_ent = [], []
            for _ in tqdm(range(runs), desc=desc):
                nsc = ns.copy(bs=[b.copy() for b in start_bs])
                best_bs.append([b.copy() for b in nsc.get_bs()])
                best_ent.append(nsc.entropy())
#                gt.mcmc_equilibrate(nsc, mcmc_args={'vertices':vlist},
# DEBUG                                    callback=cb_run)
                best_bs[-1] = self.continuous_map_nested_blockstate(
                    best_bs[-1], io_is_bs=True)
                best_ent[-1] = nsc.copy(bs=best_bs[-1]).entropy()
            primary_ent, primary_bs = sorted( zip(best_ent, best_bs),
                                              key=lambda x: x[0] )[0]
            return primary_bs, primary_ent

        bs = [b.copy() for b in ns.get_bs()]
        ent = ns.entropy()
        print('Entropy: {}'.format(ent))
        for count in range(steps):
            bs, ent = step(bs, 'Step #{}'.format(count))
            print('Entropy: {}'.format(ent))
        return bs

    def calc_chained_nested_blockstate(self, prop, matcher=None,
                                       bext='max', runs=10):
        for count in range(os.sys.maxsize):
            fname = naming.gen('chainedbstate',
                [('prop', prop),
                 (matcher, None if matcher is None else matcher.name),
                 ('run', count)], self.ext_nbs)
            try:
                os.makedirs(path.join(self.blocks_dir, fname), exist_ok=False)
                print('Reserving name: {}'.format(fname))
                break
            except OSError:
                pass

        ns = self.get_chained_nested_blockstate(
                prop=prop, matcher=matcher, bext=bext )
        bs = self.step_chained_nested_blockstate(ns, runs)
        ns = ns.copy(bs=bs)

        self.store_blockstate(fname, path.join(self.blocks_dir, fname),
                              state=ns, pclabel='type')
        print('Saved chained state: {}'.format(fname))
        self.loaded['chainedbstate'] = fname
        return fname

    def search_slice(self, slicerange, g=None, overlap=False, layers=True):
        slice_col = self.time
        if g is None:
            g = gt.load_graph(path.join(self.graph_dir, self.graph_name))

        g.ep['sliced'] = g.new_edge_property("int")
        for sval in slicerange:
            name_args = [('slice-'+slice_col, sval)]
            print('Slicing at {}'.format(sval))
            for e in g.edges():
                g.ep['sliced'][e] = g.ep[slice_col][e] > sval
            self.calc_nested_blockstate(name_args=name_args, overlap=overlap,
                g=g, state_args=dict(ec=g.ep['sliced'], layers=layers))

    ######################
    # I/O and conversion #
    ######################

    # Graph

    def list_graphs(self):
        return [fname for fname in os.listdir(self.data_dir)
                      if naming.splitext(fname)[1] == self.ext_g]

    def set_graph(self, name, extended=None):
        """
        Set graph to given name and extended params. Doesn't load the graph.

        Parameters
        ----------
        name: `str`
        extended: `dict` suitable as **kwargs to `extend_graph()`

        Returns
        -------
        `False` if the set graph did not change, otherwise `True`
        """
        assert path.isfile(path.join(self.data_dir, name, name))
        if (self.graph_name==name) and (self.graph_extended==extended):
            return False
        else:
            self.clear_graph()
            self.loaded['graph'] = name
            self.graph_extended = extended
            return True

    def load_graph(self):
        """
        Loads a graph previously set by `set_graph` into `self.graph`.
        """
        self.clear_graph()
        self.graph = gt.load_graph(path.join(self.graph_dir, self.graph_name))
        if self.graph_extended is not None:
            self.extend_graph(**self.graph_extended)

    def clear_graph(self):
        clearattrs(self, ['graph', 'graph_extended_vtype'])
        #self.graph_extended = None

    # Blockstate

    def list_blockstates(self):
        return [fname for fname in os.listdir(self.graph_dir)
                      if naming.splitext(fname)[1] == self.ext_nbs]

    def list_chainedbstates(self):
        return [fname for fname in os.listdir(self.blocks_dir)
                      if naming.splitext(fname)[1] == self.ext_nbs]

    def load_blockstate(self, chained=False):
        """
        Loads a previously stored blockstate from a json or pickle file.

        When loading a json state, `self.graph` and the state's graph
        `self.state.g` refer to the same object.

        The loaded state is found at `self.state`.

        Parameters
        ==========
        chained: uses `self.blocks_name` if False else `self.chained_name`.
        """
        self.clear_blockstate()
        blocks_name = self.chained_name if chained else self.blocks_name
        blocks_dir = self.chained_dir if chained else self.blocks_dir
        if naming.splitext(blocks_name)[1] == '.pickle':
            self.state = ioio.load(blocks_name, blocks_dir)
        else:
            obj = ioio.load(blocks_name, blocks_dir, format='json')
            cls = getattr(gt.graph_tool.inference, obj['class'])
            if self.set_graph(**obj['graph']) or not hasattr(self, 'graph'):
                self.load_graph()
            args = obj['args']
            args['bs'] = list(map(numpy.array, args['bs']))
            for key, val in obj['vp_args'].items():
                if val is not None:
                    args[key] = self.graph.vp[val]
            for key, val in obj['ep_args'].items():
                if val is not None:
                    args[key] = self.graph.ep[val]
            if args.get('ec', None) is not None:
                args['base_type'] = gt.LayeredBlockState
            else:
                args.pop('layers', None)
            self.state = cls(self.graph, **args)
        print('Loaded: {}\n{}'.format(blocks_name, self.state))

    def store_blockstate(self, fname, fdir, state=None,
                         pclabel=None, layers=None, ec=None):
        if state is None:
            state = self.state
        obj = {
            'class': type(state).__name__,
            'graph': {
                'name': self.graph_name,
                'extended': self.graph_extended
            },
            'args': {
                'bs': list(map( lambda x: list(map(int, x)),
                                state.get_bs() )),
                'layers': layers
            },
            'vp_args': {
                'pclabel': pclabel,
            },
            'ep_args': {
                'ec': ec
            },
            'entropy': state.entropy()
        }
        ioio.store(obj, fname, fdir, format='json')
        print('Stored: {}'.format(path.join(fdir, fname)))

    def clear_blockstate(self, state=True):
        clearattrs(self, ['blocks', 'wblocks', 'eblocks',
                          'blocks_levels', 'wblocks_levels', 'eblocks_levels'])
        if state:
            clearattrs(self, ['state'])

    # Blocks

    def store_blocks(self):
        fname = self.loaded['blockstate']
        for bname in ['blocks', 'wblocks', 'eblocks']:
            fpath = path.join(self.blocks_dir, self.blocks_name+bname)
            getattr(self, bname).to_json(fpath)

    def load_blocks(self):
        self.clear_blockstate(state=False)
        fname = self.loaded['blockstate']
        for bname in ['blocks', 'wblocks', 'eblocks']:
            fpath = path.join(self.blocks_dir, self.blocks_name+bname)
            try:
                setattr(self, bname, pandas.read_json(fpath))
                setattr(self, bname + '_levels',
                        [x for x in getattr(self, bname) if isinstance(x, int)])
            except FileNotFoundError:
                pass

    # Conversion

    def blockstate_to_dataframes(self, state=None):
        """
        state: the blockstate instance to be turned into dataframes indexed at
        documents and words
        """
        if state is None:
            state = self.state
        g = state.g

        df = pandas.DataFrame(index=[g.vp['name'][v] for v in g.vertices()])
        df['v'] = list(g.vertices())
        df['type'] = [g.vp['type'][v] for v in g.vertices()]
        for level in range(1, len(state.levels)): # skip trivial top level
            blocks = state.project_level(level-1).get_blocks()
            df[level] = [blocks[v] for v in g.vertices()]
        self.blocks  = df[df.type==0]
        self.wblocks = df[df.type==1]
        self.eblocks = df[df.type >1]

        # align index using self.gdocvname, otherwise trust the data order
        if self.gdocvname is not None:
            assert self.blocks.index.is_unique
            #TODO: perahps we want gdocvname to always be string?
            gdocvtype = self.data[self.gdocvname].dtype.type
            if issubclass(gdocvtype, (int, numpy.int0)):
                self.blocks.index = list(map(int, self.blocks.index))
            self.blocks = self.blocks.reindex(self.data[self.gdocvname])
        self.blocks.index = self.data.index
        self.blocks.dropna(inplace=True)

        # set up block levels lists
        self.blocks_levels = [x for x in self.blocks if isinstance(x, int)]
        self.wblocks_levels = [x for x in self.wblocks if isinstance(x, int)]
        self.eblocks_levels = [x for x in self.eblocks if isinstance(x, int)]
    
    def blocks_to_csv(self, savename,
          join=['year', 'journal', 'title', 'authors', 'affiliations']):
        """
        savename: file to save to, relative to self.analysis_dir
        blockstate_to_dataframe, or dataframe returned by said method
        """
        df = self.blocks
        # df = df.rename(columns=lambda x: 'L'+str(x) if type(x) is int else x)
        if join:
            dfj = self.data[join]
            if self.gdocvname is not None:
                dfj.index = self.data[self.gdocvname]
            df = df.join(dfj)
        df['_index'] = df.index
        df = df.sort_values(['type', '_index'])
        del df['_index']
        if 'affiliations' in df:
            df.affiliations = df.affiliations.apply(
                lambda x: x if len(repr(x))<32000 else x[0])
        df.to_csv(path.join(self.analysis_dir, savename))


