# coding: utf-8

# Abstractology - Study of the organisation and evolution of a corpus
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Project:
# <https://en.wikiversity.org/wiki/The_dynamics_and_social_organization_of
#  _innovation_in_the_field_of_oncology>
#
# Reference repository for this file:
# <https://gitlab.com/solstag/abstractology>
#
# Contributions are welcome, get in touch with the author(s).

import pandas, numpy

class CorpusParsers():
    """
    Parsers for different formats, inherited by Corporalogy.
    """
    def get_csv(self, fpath, read_csv_args={}):
        df = pandas.read_csv(fpath, **read_csv_args)
        return df

    def get_csvdir(self, csv_dir, read_csv_args={}):
        fnames = [name for name in os.listdir(csv_dir) if name.endswith('.csv')]
        df = pandas.read_csv(path.join(csv_dir, fnames.pop(0)),
                             **read_csv_args)
        for name in fnames:
            df = df.append(
                pandas.read_csv(path.join(csv_dir, name), low_memory=False),
                ignore_index=True, sort=False, **read_csv_args)
        return df

    def get_esmo(self, esmo_path):
        from lxml import html
        data = list()
        years = sorted(int(i) for i in os.listdir(esmo_path))
        # get converted PDF files
        for year in years:
            year_path = path.join(esmo_path, str(year), 'xml')
            for sec in os.listdir(year_path):
                if sec.endswith('.json'):
                    sec_path = path.join(year_path, sec)
                    with open(sec_path) as sec_file:
                        content = json.load(sec_file)
                    for i, item in enumerate(content):
                        abstext = ' '.join( l['text'] for l in item['abstract'] )
                        if not item['abstract'] or len(abstext) < 10:
                            print('Defective abstract {} at {}'.format(i, sec_path))
                            continue
                        if item['separator']!=None:
                            data.append( [abstext, year] )
        # get converted HTML files
        for year in years:
            year_path = path.join(esmo_path, str(year), 'content')
            for sec in os.listdir(year_path):
                if sec.endswith('.json'):
                    sec_path = path.join(year_path, sec)
                    with open(sec_path) as sec_file:
                        content = json.load(sec_file)
                    for a in content['abstracts']:
                        htext = html.fromstring(a['text'])
                        htext.find('h2').getparent().remove(htext.find('h2'))
                        abstract = '. '.join( [ a['title'],
                                               ' '.join(a['authors'].keys()),
                                               ' '.join(sum(a['authors'].values(),[])),
                                               htext.text_content() ] )
                        data.append( [abstract, year] )
        return pandas.DataFrame(data, columns=['original', 'year'])

    def get_getpapers(self, getpapers_path):
        data = list()
        names = [ n for n in os.listdir(getpapers_path)
                    if path.isdir(path.join(getpapers_path, n)) ]
        noabstract = []
        for name in names:
            with open(path.join(getpapers_path, name, 'eupmc_result.json')) as f:
                content = json.load(f)
            try:
                year = int(content["journalInfo"][0]["yearOfPublication"][0])
                abstract = content["abstractText"][0]
                pmid = content["id"][0]
                citations = int(content["citedByCount"][0])
                title = content["title"][0]
                if "authorList" in content and "author" in content["authorList"][0] \
                   and all( "fullName" in x for x in content["authorList"][0]["author"] ):
                    authors = [ x["fullName"][0]
                                for x in content["authorList"][0]["author"] ]
                else:
                    authors = []
                if "affiliation" in content:
                    affiliations = content["affiliation"]
                else:
                    affiliations = []
                if any( map( lambda x: len(x)!=1,
                             (content["abstractText"], content["id"],
                              content["citedByCount"], content["title"],
                              content["journalInfo"][0]["yearOfPublication"]) ) ):
                    raise Exception('Non-unique item!')
                if not len(abstract):
                    raise Exception('Empty abstract!')
                data.append([ abstract, year, pmid, citations, title, authors,
                                   affiliations])
            except KeyError:
                noabstract.append(name)
        if noabstract:
            print('Warning: In <'+getpapers_path+'> missing "abstractText":',
                  ', '.join(noabstract))
        df = pandas.DataFrame( data, columns=['original', 'year', 'pmid',
                                 'epmc_citations', 'title', 'authors', 'affiliations'] )
        pmid_sorted_idx = df.pmid.astype(int).sort_values().index
        df = df.loc[pmid_sorted_idx].sort_values('year', kind='mergesort')
        return df

    def get_pubmed(self, pubmed_file):
        from lxml import etree
        data = list()
        ctx = etree.iterparse(pubmed_file, tag='PubmedArticle')
        article = 'MedlineCitation/Article/'
        print()
        for a, e in ctx:
            pmid = str(e.xpath('MedlineCitation/PMID/text()')[0])
            print('\rExtracting pmid: {:17}'.format(pmid), end='')

            year_j = e.xpath(article+'Journal/JournalIssue/PubDate/Year/text()')
            year_m = e.xpath(article+'Journal/JournalIssue/PubDate/MedlineDate/text()')
            year_a = e.xpath(article+'ArticleDate/Year/text()')
            assert len(year_j) < 2 and len(year_m) < 2 and len(year_a) < 2
            year_j, year_m, year_a = map(''.join, (year_j, year_m, year_a))
            years = [ int(y) for y in (year_j, year_a) if y != '' ]
            if year_m != '':
                years.append(int(year_m.split()[0].split('-')[-1]))
            year = min(years)

            abstract = e.xpath(article+'Abstract/AbstractText/text()')
            if not abstract:
                continue
            abstract=''.join(abstract)

            title = str(e.xpath(article+'ArticleTitle/text()')[0])

            authors = list(); affiliations = list()
            for ee in e.xpath(article+'AuthorList/Author'):
                if ee.xpath('CollectiveName'):
                    continue
                authors.append(' '.join( ee.xpath('ForeName/text()') + \
                                         ee.xpath('LastName/text()') ))
                affiliations.extend(
                  map(str, ee.xpath('AffiliationInfo/Affiliation/text()')) )

            language = list(map(str, e.xpath(article+'Language/text()')))

            journal = e.xpath(article+'Journal/ISOAbbreviation/text()')
            if journal:
                journal = str(journal[0])
            else:
                journal = str(e.xpath(article+'Journal/Title/text()')[0])

            data.append([abstract, year, pmid, title, authors,
                         affiliations, language, journal])

            e.clear() # clean up children
            while e.getprevious() is not None:
                del e.getparent()[0]  # clean up preceding
        print()
        df = pandas.DataFrame(data, columns=['original', 'year', 'pmid', 'title',
                                'authors', 'affiliations', 'language', 'journal'])
        pmid_sorted_idx = df.pmid.astype(int).sort_values().index
        df = df.loc[pmid_sorted_idx].sort_values('year', kind='mergesort')
        return df

    def get_getpapersdir(self, getpapers_store):
        fpaths = [path.join(getpapers_store, name)
                   for name in os.listdir(getpapers_store)
                   if path.isdir(path.join(getpapers_store, name))]
        return pandas.concat( (self.get_getpapers(fpath) for fpath in fpaths),
                              ignore_index=True )

    def get_cortext(self, ctx_file):
        import sqlite3

        with sqlite3.connect(ctx_file) as con:
            tables_sql='select name from sqlite_master where type="table"'
            tables = [ x[0] for x in con.execute(tables_sql).fetchall() ]
            #print(tables)
            df = pandas.DataFrame()
            for t in tables:
                cols = pandas.read_sql_query('select * from {} limit 0'
                                             .format(t), con).columns
                if 'id' not in cols or 'data' not in cols :
                    continue
                sql = ('select id, group_concat(data," | ") as data from {}'
                       ' group by id'.format(t))
                dftmp = pandas.read_sql_query(sql, con)
                dftmp.set_index('id', inplace=True, verify_integrity=True)
                dftmp = dftmp.rename(columns={'data': t})
                df = df.join(dftmp, how='outer')

        return df

